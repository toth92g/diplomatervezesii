﻿namespace UserApplication
{
    partial class SensorDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.temperatureChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.protocolLabel = new System.Windows.Forms.Label();
            this.ipPortContainerPanel = new System.Windows.Forms.Panel();
            this.ipPortPanel = new System.Windows.Forms.Panel();
            this.ipLabel = new System.Windows.Forms.Label();
            this.portLabel = new System.Windows.Forms.Label();
            this.ipPortValuePanel = new System.Windows.Forms.Panel();
            this.ipValueLabel = new System.Windows.Forms.Label();
            this.portValueLabel = new System.Windows.Forms.Label();
            this.topPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chartAndCurrentDataContainerPanel = new System.Windows.Forms.Panel();
            this.humidityChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.currentDataPanel = new System.Windows.Forms.Panel();
            this.createBitmapButton = new System.Windows.Forms.Button();
            this.restartButton = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.debugReportButton = new System.Windows.Forms.Button();
            this.ledPictureBox = new System.Windows.Forms.PictureBox();
            this.humidityValueLabel = new System.Windows.Forms.Label();
            this.humidityLabel = new System.Windows.Forms.Label();
            this.temperatureValueLabel = new System.Windows.Forms.Label();
            this.temperatureLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.MotionLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.temperatureChart)).BeginInit();
            this.ipPortContainerPanel.SuspendLayout();
            this.ipPortPanel.SuspendLayout();
            this.ipPortValuePanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.chartAndCurrentDataContainerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.humidityChart)).BeginInit();
            this.currentDataPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // temperatureChart
            // 
            this.temperatureChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.temperatureChart.BackColor = System.Drawing.Color.WhiteSmoke;
            this.temperatureChart.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Left;
            chartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisX.ScrollBar.Size = 16D;
            chartArea1.AxisX.Title = "Time(s)";
            chartArea1.AxisY.Interval = 10D;
            chartArea1.AxisY.Maximum = 100D;
            chartArea1.AxisY.Minimum = -20D;
            chartArea1.AxisY.Title = "Temperature (°C)";
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.Name = "ChartArea1";
            this.temperatureChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.temperatureChart.Legends.Add(legend1);
            this.temperatureChart.Location = new System.Drawing.Point(68, 20);
            this.temperatureChart.Margin = new System.Windows.Forms.Padding(2);
            this.temperatureChart.Name = "temperatureChart";
            series1.BorderWidth = 4;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.IsVisibleInLegend = false;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.YValuesPerPoint = 2;
            this.temperatureChart.Series.Add(series1);
            this.temperatureChart.Size = new System.Drawing.Size(565, 239);
            this.temperatureChart.TabIndex = 5;
            this.temperatureChart.Text = "temperatureChart";
            // 
            // protocolLabel
            // 
            this.protocolLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.protocolLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.protocolLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F, System.Drawing.FontStyle.Bold);
            this.protocolLabel.Location = new System.Drawing.Point(342, 18);
            this.protocolLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.protocolLabel.Name = "protocolLabel";
            this.protocolLabel.Size = new System.Drawing.Size(282, 55);
            this.protocolLabel.TabIndex = 0;
            this.protocolLabel.Text = "Default";
            this.protocolLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ipPortContainerPanel
            // 
            this.ipPortContainerPanel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ipPortContainerPanel.Controls.Add(this.ipPortPanel);
            this.ipPortContainerPanel.Controls.Add(this.ipPortValuePanel);
            this.ipPortContainerPanel.Location = new System.Drawing.Point(407, 76);
            this.ipPortContainerPanel.Margin = new System.Windows.Forms.Padding(2);
            this.ipPortContainerPanel.Name = "ipPortContainerPanel";
            this.ipPortContainerPanel.Size = new System.Drawing.Size(150, 42);
            this.ipPortContainerPanel.TabIndex = 8;
            // 
            // ipPortPanel
            // 
            this.ipPortPanel.Controls.Add(this.ipLabel);
            this.ipPortPanel.Controls.Add(this.portLabel);
            this.ipPortPanel.Location = new System.Drawing.Point(2, 2);
            this.ipPortPanel.Margin = new System.Windows.Forms.Padding(2);
            this.ipPortPanel.Name = "ipPortPanel";
            this.ipPortPanel.Size = new System.Drawing.Size(52, 32);
            this.ipPortPanel.TabIndex = 6;
            // 
            // ipLabel
            // 
            this.ipLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipLabel.Location = new System.Drawing.Point(0, 0);
            this.ipLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ipLabel.Name = "ipLabel";
            this.ipLabel.Size = new System.Drawing.Size(52, 14);
            this.ipLabel.TabIndex = 1;
            this.ipLabel.Text = "IP";
            // 
            // portLabel
            // 
            this.portLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.portLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.portLabel.Location = new System.Drawing.Point(0, 18);
            this.portLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(52, 14);
            this.portLabel.TabIndex = 2;
            this.portLabel.Text = "PORT";
            // 
            // ipPortValuePanel
            // 
            this.ipPortValuePanel.Controls.Add(this.ipValueLabel);
            this.ipPortValuePanel.Controls.Add(this.portValueLabel);
            this.ipPortValuePanel.Location = new System.Drawing.Point(58, 2);
            this.ipPortValuePanel.Margin = new System.Windows.Forms.Padding(2);
            this.ipPortValuePanel.Name = "ipPortValuePanel";
            this.ipPortValuePanel.Size = new System.Drawing.Size(89, 32);
            this.ipPortValuePanel.TabIndex = 7;
            // 
            // ipValueLabel
            // 
            this.ipValueLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ipValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipValueLabel.Location = new System.Drawing.Point(0, 0);
            this.ipValueLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ipValueLabel.Name = "ipValueLabel";
            this.ipValueLabel.Size = new System.Drawing.Size(89, 14);
            this.ipValueLabel.TabIndex = 3;
            this.ipValueLabel.Text = "Default";
            // 
            // portValueLabel
            // 
            this.portValueLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.portValueLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.portValueLabel.Location = new System.Drawing.Point(0, 18);
            this.portValueLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.portValueLabel.Name = "portValueLabel";
            this.portValueLabel.Size = new System.Drawing.Size(89, 14);
            this.portValueLabel.TabIndex = 4;
            this.portValueLabel.Text = "Default";
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(246)))), ((int)(((byte)(246)))));
            this.topPanel.Controls.Add(this.protocolLabel);
            this.topPanel.Controls.Add(this.ipPortContainerPanel);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Margin = new System.Windows.Forms.Padding(2);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(962, 119);
            this.topPanel.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.panel1.Location = new System.Drawing.Point(69, 12);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(825, 5);
            this.panel1.TabIndex = 9;
            // 
            // bottomPanel
            // 
            this.bottomPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(246)))), ((int)(((byte)(246)))));
            this.bottomPanel.Controls.Add(this.panel1);
            this.bottomPanel.Controls.Add(this.panel2);
            this.bottomPanel.Controls.Add(this.chartAndCurrentDataContainerPanel);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomPanel.Location = new System.Drawing.Point(0, 119);
            this.bottomPanel.Margin = new System.Windows.Forms.Padding(2);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(962, 490);
            this.bottomPanel.TabIndex = 10;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.panel2.Location = new System.Drawing.Point(69, 568);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(825, 5);
            this.panel2.TabIndex = 10;
            // 
            // chartAndCurrentDataContainerPanel
            // 
            this.chartAndCurrentDataContainerPanel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.chartAndCurrentDataContainerPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.chartAndCurrentDataContainerPanel.Controls.Add(this.humidityChart);
            this.chartAndCurrentDataContainerPanel.Controls.Add(this.temperatureChart);
            this.chartAndCurrentDataContainerPanel.Controls.Add(this.currentDataPanel);
            this.chartAndCurrentDataContainerPanel.Location = new System.Drawing.Point(69, 20);
            this.chartAndCurrentDataContainerPanel.Margin = new System.Windows.Forms.Padding(2);
            this.chartAndCurrentDataContainerPanel.Name = "chartAndCurrentDataContainerPanel";
            this.chartAndCurrentDataContainerPanel.Size = new System.Drawing.Size(825, 544);
            this.chartAndCurrentDataContainerPanel.TabIndex = 6;
            // 
            // humidityChart
            // 
            this.humidityChart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.humidityChart.BackColor = System.Drawing.Color.WhiteSmoke;
            this.humidityChart.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Left;
            chartArea2.AxisX.ScrollBar.Size = 16D;
            chartArea2.AxisX.Title = "Time(s)";
            chartArea2.AxisY.Interval = 10D;
            chartArea2.AxisY.Maximum = 100D;
            chartArea2.AxisY.Minimum = 0D;
            chartArea2.AxisY.Title = "Humidity (%)";
            chartArea2.CursorX.IsUserEnabled = true;
            chartArea2.CursorX.IsUserSelectionEnabled = true;
            chartArea2.Name = "ChartArea1";
            this.humidityChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.humidityChart.Legends.Add(legend2);
            this.humidityChart.Location = new System.Drawing.Point(68, 280);
            this.humidityChart.Margin = new System.Windows.Forms.Padding(2);
            this.humidityChart.Name = "humidityChart";
            series2.BorderWidth = 4;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.IsVisibleInLegend = false;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            series2.YValuesPerPoint = 2;
            this.humidityChart.Series.Add(series2);
            this.humidityChart.Size = new System.Drawing.Size(565, 239);
            this.humidityChart.TabIndex = 6;
            this.humidityChart.Text = "humidityChart";
            // 
            // currentDataPanel
            // 
            this.currentDataPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.currentDataPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.currentDataPanel.Controls.Add(this.createBitmapButton);
            this.currentDataPanel.Controls.Add(this.restartButton);
            this.currentDataPanel.Controls.Add(this.pauseButton);
            this.currentDataPanel.Controls.Add(this.startButton);
            this.currentDataPanel.Controls.Add(this.debugReportButton);
            this.currentDataPanel.Controls.Add(this.ledPictureBox);
            this.currentDataPanel.Controls.Add(this.humidityValueLabel);
            this.currentDataPanel.Controls.Add(this.humidityLabel);
            this.currentDataPanel.Controls.Add(this.temperatureValueLabel);
            this.currentDataPanel.Controls.Add(this.temperatureLabel);
            this.currentDataPanel.Controls.Add(this.label1);
            this.currentDataPanel.Controls.Add(this.MotionLabel);
            this.currentDataPanel.Location = new System.Drawing.Point(653, 20);
            this.currentDataPanel.Margin = new System.Windows.Forms.Padding(2);
            this.currentDataPanel.Name = "currentDataPanel";
            this.currentDataPanel.Size = new System.Drawing.Size(136, 499);
            this.currentDataPanel.TabIndex = 2;
            // 
            // createBitmapButton
            // 
            this.createBitmapButton.Location = new System.Drawing.Point(26, 248);
            this.createBitmapButton.Margin = new System.Windows.Forms.Padding(2);
            this.createBitmapButton.Name = "createBitmapButton";
            this.createBitmapButton.Size = new System.Drawing.Size(83, 27);
            this.createBitmapButton.TabIndex = 9;
            this.createBitmapButton.Text = "Create Bitmap";
            this.createBitmapButton.UseVisualStyleBackColor = true;
            this.createBitmapButton.Click += new System.EventHandler(this.createBitmapButton_Click);
            // 
            // restartButton
            // 
            this.restartButton.Image = global::UserApplication.Properties.Resources.clear_button;
            this.restartButton.Location = new System.Drawing.Point(4, 420);
            this.restartButton.Margin = new System.Windows.Forms.Padding(2);
            this.restartButton.Name = "restartButton";
            this.restartButton.Size = new System.Drawing.Size(128, 61);
            this.restartButton.TabIndex = 8;
            this.restartButton.UseVisualStyleBackColor = true;
            this.restartButton.Click += new System.EventHandler(this.restartButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.Image = global::UserApplication.Properties.Resources.pause_button;
            this.pauseButton.Location = new System.Drawing.Point(4, 350);
            this.pauseButton.Margin = new System.Windows.Forms.Padding(2);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(128, 61);
            this.pauseButton.TabIndex = 7;
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // startButton
            // 
            this.startButton.Image = global::UserApplication.Properties.Resources.start_button;
            this.startButton.Location = new System.Drawing.Point(4, 279);
            this.startButton.Margin = new System.Windows.Forms.Padding(2);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(128, 61);
            this.startButton.TabIndex = 7;
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // debugReportButton
            // 
            this.debugReportButton.Location = new System.Drawing.Point(26, 212);
            this.debugReportButton.Margin = new System.Windows.Forms.Padding(2);
            this.debugReportButton.Name = "debugReportButton";
            this.debugReportButton.Size = new System.Drawing.Size(83, 27);
            this.debugReportButton.TabIndex = 6;
            this.debugReportButton.Text = "Debug Report";
            this.debugReportButton.UseVisualStyleBackColor = true;
            this.debugReportButton.Click += new System.EventHandler(this.debugReportButton_Click);
            // 
            // ledPictureBox
            // 
            this.ledPictureBox.ErrorImage = null;
            this.ledPictureBox.InitialImage = null;
            this.ledPictureBox.Location = new System.Drawing.Point(46, 39);
            this.ledPictureBox.Margin = new System.Windows.Forms.Padding(2);
            this.ledPictureBox.MaximumSize = new System.Drawing.Size(50, 50);
            this.ledPictureBox.MinimumSize = new System.Drawing.Size(50, 50);
            this.ledPictureBox.Name = "ledPictureBox";
            this.ledPictureBox.Size = new System.Drawing.Size(50, 50);
            this.ledPictureBox.TabIndex = 5;
            this.ledPictureBox.TabStop = false;
            // 
            // humidityValueLabel
            // 
            this.humidityValueLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.humidityValueLabel.Location = new System.Drawing.Point(46, 182);
            this.humidityValueLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.humidityValueLabel.Name = "humidityValueLabel";
            this.humidityValueLabel.Size = new System.Drawing.Size(50, 15);
            this.humidityValueLabel.TabIndex = 4;
            this.humidityValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // humidityLabel
            // 
            this.humidityLabel.AutoSize = true;
            this.humidityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.humidityLabel.Location = new System.Drawing.Point(37, 150);
            this.humidityLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.humidityLabel.Name = "humidityLabel";
            this.humidityLabel.Size = new System.Drawing.Size(70, 17);
            this.humidityLabel.TabIndex = 3;
            this.humidityLabel.Text = "Humidity";
            // 
            // temperatureValueLabel
            // 
            this.temperatureValueLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.temperatureValueLabel.Location = new System.Drawing.Point(46, 125);
            this.temperatureValueLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.temperatureValueLabel.Name = "temperatureValueLabel";
            this.temperatureValueLabel.Size = new System.Drawing.Size(50, 15);
            this.temperatureValueLabel.TabIndex = 2;
            this.temperatureValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // temperatureLabel
            // 
            this.temperatureLabel.AutoSize = true;
            this.temperatureLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temperatureLabel.Location = new System.Drawing.Point(23, 93);
            this.temperatureLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.temperatureLabel.Name = "temperatureLabel";
            this.temperatureLabel.Size = new System.Drawing.Size(101, 17);
            this.temperatureLabel.TabIndex = 1;
            this.temperatureLabel.Text = "Temperature";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Motion";
            // 
            // MotionLabel
            // 
            this.MotionLabel.AutoSize = true;
            this.MotionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MotionLabel.Location = new System.Drawing.Point(43, 7);
            this.MotionLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.MotionLabel.Name = "MotionLabel";
            this.MotionLabel.Size = new System.Drawing.Size(56, 17);
            this.MotionLabel.TabIndex = 0;
            this.MotionLabel.Text = "Motion";
            // 
            // SensorDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 609);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.topPanel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(866, 597);
            this.Name = "SensorDataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SensorDataForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SensorDataForm_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.temperatureChart)).EndInit();
            this.ipPortContainerPanel.ResumeLayout(false);
            this.ipPortPanel.ResumeLayout(false);
            this.ipPortValuePanel.ResumeLayout(false);
            this.topPanel.ResumeLayout(false);
            this.bottomPanel.ResumeLayout(false);
            this.bottomPanel.PerformLayout();
            this.chartAndCurrentDataContainerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.humidityChart)).EndInit();
            this.currentDataPanel.ResumeLayout(false);
            this.currentDataPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ledPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataVisualization.Charting.Chart temperatureChart;
        private System.Windows.Forms.Label protocolLabel;
        private System.Windows.Forms.Panel ipPortContainerPanel;
        private System.Windows.Forms.Panel ipPortPanel;
        private System.Windows.Forms.Label ipLabel;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.Panel ipPortValuePanel;
        private System.Windows.Forms.Label ipValueLabel;
        private System.Windows.Forms.Label portValueLabel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Panel chartAndCurrentDataContainerPanel;
        private System.Windows.Forms.Label temperatureLabel;
        private System.Windows.Forms.Label MotionLabel;
        private System.Windows.Forms.Panel currentDataPanel;
        private System.Windows.Forms.Label humidityLabel;
        private System.Windows.Forms.Label temperatureValueLabel;
        private System.Windows.Forms.Label humidityValueLabel;
        private System.Windows.Forms.PictureBox ledPictureBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart humidityChart;
        private System.Windows.Forms.Button debugReportButton;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Button restartButton;
        private System.Windows.Forms.Button createBitmapButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
    }
}