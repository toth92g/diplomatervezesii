﻿namespace UserApplication
{
    using System;
    using System.Windows.Forms;
    using System.Windows.Forms.DataVisualization.Charting;
    using System.Runtime.InteropServices;
    using System.Text;
    using CoAP;
    using MqttBroker;
    using MqttClient;
    using Utilities;

    /// <summary>
    /// This form initializes the communication procol, gets the data, and shows it.
    /// </summary>
    public partial class SensorDataForm : Form
    {
        Timer timer = null;
        int tickCounter = 0;
        int timerIntervalMillisec;
        int numberOfChartValues;
        Protocols protocol;
        string outputDirectory = "Output";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nLeftRect"></param>
        /// <param name="nTopRect"></param>
        /// <param name="nRightRect"></param>
        /// <param name="nBottomRect"></param>
        /// <param name="nWidthEllipse"></param>
        /// <param name="nHeightEllipse"></param>
        /// <returns></returns>
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
        int nLeftRect, // x-coordinate of upper-left corner
        int nTopRect, // y-coordinate of upper-left corner
        int nRightRect, // x-coordinate of lower-right corner
        int nBottomRect, // y-coordinate of lower-right corner
        int nWidthEllipse, // height of ellipse
        int nHeightEllipse // width of ellipse
        );

        /// <summary>
        /// Constructor for the sensor data form.
        /// </summary>
        /// <param name="protocol"></param>
        /// <param name="timerIntervalMillisec"></param>
        public SensorDataForm(Protocols protocol, int timerIntervalMillisec = 100, int numberOfChartValues = 100)
        {
            InitializeComponent();

            // Setting round corner for the regions
            chartAndCurrentDataContainerPanel.Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, chartAndCurrentDataContainerPanel.Width, chartAndCurrentDataContainerPanel.Height, 25, 25));
            temperatureChart.Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, temperatureChart.Width, temperatureChart.Height, 50, 50));
            humidityChart.Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, humidityChart.Width, humidityChart.Height, 50, 50));
            currentDataPanel.Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, currentDataPanel.Width, currentDataPanel.Height, 50, 50));

            this.protocol = protocol;
            this.timerIntervalMillisec = timerIntervalMillisec;
            this.numberOfChartValues = numberOfChartValues;

            temperatureChart.ChartAreas[0].AxisX.Minimum = 0;
            temperatureChart.ChartAreas[0].AxisX.Maximum = numberOfChartValues;
            temperatureChart.ChartAreas[0].AxisX.Interval = numberOfChartValues / 10;

            humidityChart.ChartAreas[0].AxisX.Minimum = 0;
            humidityChart.ChartAreas[0].AxisX.Maximum = numberOfChartValues;
            humidityChart.ChartAreas[0].AxisX.Interval = numberOfChartValues / 10;

            startButton.Enabled = false;

            try
            {
                switch (protocol)
                {
                    case Protocols.CoAP:
                        CoAPServer.StartServer();
                        protocolLabel.Text = "CoAP server is running";
                        ipValueLabel.Text = CoAPServer.ownIP.ToString();
                        portValueLabel.Text = CoAPServer.ownPort;
                        break;
                    case Protocols.MQTT:
                        MQTTBroker.StartBroker();
                        MQTTClient.StartClient();
                        MQTTClient.Subscribe("/temperature", 0);
                        MQTTClient.Subscribe("/humidity", 0);
                        MQTTClient.Subscribe("/motion", 0);
                        protocolLabel.Text = "MQTT broker is running";
                        ipValueLabel.Text = MQTTBroker.ownIP.ToString();
                        portValueLabel.Text = MQTTBroker.ownPort;
                        break;
                }

                timer = new Timer();
                timer.Interval = (timerIntervalMillisec);
                timer.Tick += new EventHandler(TimerTick);
                timer.Start();
            }
            catch(Exception e)
            {
                Console.WriteLine("Could not start the MQTT client. \n Error message: {0}", e.Message);
                return;
            }
        }

        /// <summary>
        /// Before exiting the form and the application the timer is getting stopped and disposed 
        /// and then the application processes are terminated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SensorDataForm_Closing(object sender, EventArgs e)
        {
            timer.Stop();
            timer.Dispose(); 
            System.Diagnostics.Process.GetCurrentProcess().Kill();  // Can't close MQTT with Application.Exit() so i chose the radical version
        }

        /// <summary>
        /// Timer ticker function for the event handling.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerTick(object sender, EventArgs e)
        {
            string temperature = null;
            string humidity = null;
            bool motion = false;
            tickCounter++;

            // Saving the incoming sensor information
            switch (protocol)
            {
                
                case Protocols.CoAP:
                    if (CoAPServer.temperatureBuffer == null)   // TODO: it should never happen, but...
                        return;
                    if(CoAPServer.temperatureBuffer.Count != 0)
                        temperature = CoAPServer.temperatureBuffer.Dequeue();
                    if (CoAPServer.humidityBuffer.Count != 0)
                        humidity = CoAPServer.humidityBuffer.Dequeue();
                    motion = CoAPServer.motionBuffer;

                    if(tickCounter % 10 == 0)   // After a while the led needs to be reseted
                        CoAPServer.motionBuffer = false;
                    break;
                case Protocols.MQTT:
                    if (MQTTClient.temperatureBuffer == null)   // TODO: it should never happen, but...
                        return;
                    if (MQTTClient.temperatureBuffer.Count != 0)
                        temperature = MQTTClient.temperatureBuffer.Dequeue();
                    if (MQTTClient.humidityBuffer.Count != 0)
                        humidity = MQTTClient.humidityBuffer.Dequeue();
                    motion = MQTTClient.motionBuffer;

                    if (tickCounter % 10 == 0)   // After a while the led needs to be reseted
                        MQTTClient.motionBuffer = false;
                    break;
            }

            // If chart updating is not paused.
            if (!startButton.Enabled)
            {
                // Addig a new point to the temperature chart whether new data arrived or the latest is used.
                if (temperature == null)
                {
                    if (temperatureChart.Series[0].Points.Count == 0)       // TODO: EXCEPTION, ha csak ugy kiikszelem
                        temperatureChart.Series[0].Points.Add(new DataPoint(0, 0));
                    else
                    {
                        DataPoint lastPoint = temperatureChart.Series[0].Points[temperatureChart.Series[0].Points.Count - 1];
                        temperatureChart.Series[0].Points.Add(new DataPoint(lastPoint.XValue + 1, lastPoint.YValues[0]));
                    }
                }
                else
                {
                    temperatureChart.Series[0].Points.Add(new DataPoint(temperatureChart.Series[0].Points.Count, Int32.Parse(temperature)));
                    temperatureValueLabel.Text = temperature + "°C";
                }
            
                // Addig a new point to the humidity chart whether new data arrived or the latest is used.
                if (humidity == null)
                {
                    if (humidityChart.Series[0].Points.Count == 0)
                        humidityChart.Series[0].Points.Add(new DataPoint(0, 0));
                    else
                    {
                        DataPoint lastPoint = humidityChart.Series[0].Points[humidityChart.Series[0].Points.Count - 1];
                        humidityChart.Series[0].Points.Add(new DataPoint(lastPoint.XValue + 1, lastPoint.YValues[0]));
                    }
                }
                else
                {
                    humidityChart.Series[0].Points.Add(new DataPoint(humidityChart.Series[0].Points.Count, Int32.Parse(humidity)));
                    humidityValueLabel.Text = humidity + "%";
                }
            
                // We only want to move the X axis of the chart if we have enough data (more than the numberOfChartValues).
                // Its the same for both charts.
                if(temperatureChart.Series[0].Points.Count > 60)
                {
                    temperatureChart.ChartAreas[0].AxisX.Minimum = temperatureChart.Series[0].Points.Count + 40 - numberOfChartValues;
                    temperatureChart.ChartAreas[0].AxisX.Maximum = temperatureChart.Series[0].Points.Count + 40;
                    humidityChart.ChartAreas[0].AxisX.Minimum = humidityChart.Series[0].Points.Count + 40 - numberOfChartValues;
                    humidityChart.ChartAreas[0].AxisX.Maximum = humidityChart.Series[0].Points.Count + 40;
                }
            }

            if (motion)
                ledPictureBox.Image = Properties.Resources.green_led;
            else
                ledPictureBox.Image = Properties.Resources.red_led;
        }

        /// <summary>
        /// Pushing the button creates the log file of the temperature and humidity datas.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void debugReportButton_Click(object sender, EventArgs e)
        {
            System.IO.Directory.CreateDirectory(outputDirectory);
            string identifier = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString();
            StringBuilder builder = new StringBuilder();

            for (int i = 1; i < temperatureChart.Series[0].Points.Count; i++)   // First line in temperatureChart has double zero y values
            {
                builder.AppendLine(i.ToString() + "," + temperatureChart.Series[0].Points[i].YValues[0].ToString() + "," + humidityChart.Series[0].Points[i].YValues[0].ToString());
            }
            System.IO.File.WriteAllText(outputDirectory + @"\log" + identifier + ".txt", builder.ToString());
        }

        /// <summary>
        /// Pushing the button starts again the chart redrawing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = false;
            pauseButton.Enabled = true;
        }

        /// <summary>
        /// The button pauses the drawing of the chart. The incoming datas are still saved for later use.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pauseButton_Click(object sender, EventArgs e)
        {
            pauseButton.Enabled = false;
            startButton.Enabled = true;
        }

        /// <summary>
        /// Pushing the button deletes all the sensor datas and clears the chart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void restartButton_Click(object sender, EventArgs e)
        {
            // Removes all points from the charts.
            while (temperatureChart.Series[0].Points.Count > 0)
            {
                temperatureChart.Series[0].Points.RemoveAt(0);
                humidityChart.Series[0].Points.RemoveAt(0);
            }

            temperatureChart.Series[0].Points.Add(0,0);
            humidityChart.Series[0].Points.Add(0, 0);

            // Clears the current value labels.
            temperatureValueLabel.Text = "                 ";
            humidityValueLabel.Text = "           ";

            // Sets the x axis to their initial states.
            temperatureChart.ChartAreas[0].AxisX.Minimum = 0;
            temperatureChart.ChartAreas[0].AxisX.Maximum = numberOfChartValues;

            humidityChart.ChartAreas[0].AxisX.Minimum = 0;
            humidityChart.ChartAreas[0].AxisX.Maximum = numberOfChartValues;

            // Reset the motion led
            ledPictureBox.Image = Properties.Resources.red_led;
        }

        /// <summary>
        /// Creates the images of the charts.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createBitmapButton_Click(object sender, EventArgs e)
        {
            string identifier = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString();
            System.IO.Directory.CreateDirectory(outputDirectory);
            temperatureChart.SaveImage(outputDirectory + @"\temperature_" + identifier + ".png", ChartImageFormat.Png);
            humidityChart.SaveImage(outputDirectory + @"\humidity_" + identifier + ".png", ChartImageFormat.Png);
        }
    }
}
