﻿namespace UserApplication
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CoapButton = new System.Windows.Forms.Button();
            this.MqttButton = new System.Windows.Forms.Button();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.HowToButton = new System.Windows.Forms.Button();
            this.InfoButton = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // CoapButton
            // 
            this.CoapButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CoapButton.Location = new System.Drawing.Point(67, 92);
            this.CoapButton.Margin = new System.Windows.Forms.Padding(2);
            this.CoapButton.Name = "CoapButton";
            this.CoapButton.Size = new System.Drawing.Size(145, 75);
            this.CoapButton.TabIndex = 0;
            this.CoapButton.Text = "CoAP";
            this.CoapButton.UseVisualStyleBackColor = true;
            this.CoapButton.Click += new System.EventHandler(this.CoapButton_Click);
            // 
            // MqttButton
            // 
            this.MqttButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.MqttButton.Location = new System.Drawing.Point(235, 92);
            this.MqttButton.Margin = new System.Windows.Forms.Padding(2);
            this.MqttButton.Name = "MqttButton";
            this.MqttButton.Size = new System.Drawing.Size(145, 75);
            this.MqttButton.TabIndex = 2;
            this.MqttButton.Text = "MQTT";
            this.MqttButton.UseVisualStyleBackColor = true;
            this.MqttButton.Click += new System.EventHandler(this.MqttButton_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            this.fileSystemWatcher1.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Changed);
            // 
            // HowToButton
            // 
            this.HowToButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HowToButton.Location = new System.Drawing.Point(67, 184);
            this.HowToButton.Margin = new System.Windows.Forms.Padding(2);
            this.HowToButton.Name = "HowToButton";
            this.HowToButton.Size = new System.Drawing.Size(145, 75);
            this.HowToButton.TabIndex = 3;
            this.HowToButton.Text = "How to";
            this.HowToButton.UseVisualStyleBackColor = true;
            this.HowToButton.Click += new System.EventHandler(this.HowToButton_Click);
            // 
            // InfoButton
            // 
            this.InfoButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.InfoButton.Location = new System.Drawing.Point(235, 184);
            this.InfoButton.Margin = new System.Windows.Forms.Padding(2);
            this.InfoButton.Name = "InfoButton";
            this.InfoButton.Size = new System.Drawing.Size(145, 75);
            this.InfoButton.TabIndex = 4;
            this.InfoButton.Text = "Info";
            this.InfoButton.UseVisualStyleBackColor = true;
            this.InfoButton.Click += new System.EventHandler(this.InfoButton_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 362);
            this.Controls.Add(this.InfoButton);
            this.Controls.Add(this.HowToButton);
            this.Controls.Add(this.MqttButton);
            this.Controls.Add(this.CoapButton);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximumSize = new System.Drawing.Size(450, 400);
            this.MinimumSize = new System.Drawing.Size(450, 400);
            this.Name = "Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button CoapButton;
        private System.Windows.Forms.Button MqttButton;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Button InfoButton;
        private System.Windows.Forms.Button HowToButton;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}

