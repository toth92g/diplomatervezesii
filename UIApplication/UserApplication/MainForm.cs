﻿namespace UserApplication
{
    using System;
    using System.Windows.Forms;
    using Utilities;

    /// <summary>
    /// Form of the main window. This is the entry point of the GUI.
    /// </summary>
    public partial class Main : Form
    {
        string name = "Tóth Gábor";
        string neptun = "OVHQY1";
        string date = "2017/2018";
        /// <summary>
        /// Constructor for the main window.
        /// </summary>
        public Main()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Load function for the main window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Button event, that starts the chosen communication.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CoapButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            new SensorDataForm(Protocols.CoAP).Show();
        }

        /// <summary>
        /// Button event, that starts the chosen communication.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MqttButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            new SensorDataForm(Protocols.MQTT).Show();
        }

        /// <summary>
        /// Event for the user guide button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HowToButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Choose a protocol...");
        }

        /// <summary>
        /// Event for the personal data button click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InfoButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Name:\t\t" + name + "\r\nNeptun:\t\t" + neptun + "\r\nDate\t\t" + date);
        }

        /// <summary>
        /// Listens to the file system change notifications and raises events when a directory, or file in a directory, changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileSystemWatcher1_Changed(object sender, System.IO.FileSystemEventArgs e)
        {

        }
    }
}
