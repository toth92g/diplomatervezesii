﻿namespace MqttClient
{
    using System;
    using System.Text;
    using System.Net;
    using System.Net.Sockets;
    using System.Linq;
    using System.Collections.Generic;
    using M2Mqtt.Networking.M2Mqtt;
    using M2Mqtt.Networking.M2Mqtt.Messages;

    /// <summary>
    /// MQTT client class by M2MQTT.
    /// </summary>
    public static class MQTTClient
    {
        public static Queue<string> temperatureBuffer;
        public static Queue<string> humidityBuffer;
        public static bool motionBuffer;

        static IPAddress ownIP;
        static MqttClient client;
        static string clientId;

        /// <summary>
        /// The functions creates the client and tries to connect to the local broker.
        /// </summary>
        public static void StartClient()
        {
            temperatureBuffer = new Queue<string>();
            humidityBuffer = new Queue<string>();

            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            ownIP = localIPs.First(x => x.AddressFamily == AddressFamily.InterNetwork);

            #pragma warning disable CS0618          // Suppressed warning
            client = new MqttClient(ownIP);
            clientId = Guid.NewGuid().ToString();   // Unique ID at each relaunch of the application

            try
            {
                client.Connect(clientId);
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not start the MQTT client. \n Error message: {0}", e.Message);
                return;
            }
            Console.WriteLine("MQTT Client is started.");
            client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
        }

        /// <summary>
        /// This function stops the client and disposes the client class instance.
        /// </summary>
        public static void StopClient()
        {
            // TODO: garbage collection
        }

        /// <summary>
        /// The function subscribes to the given topic with the QoS.
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="QoS"></param>
        public static void Subscribe(string topic, byte QoS = 0)
        {
            client.Subscribe(new string[] { topic }, new byte[] { QoS });
            Console.WriteLine("MQTT Client is subscribed to topic: {0}.", topic);
        }

        /// <summary>
        /// The function publishes the given message to the topic with the QoS.
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="message"></param>
        /// <param name="QoS"></param>
        public static void Publish(string topic, string message, byte QoS = 0)
        {
            client.Publish(topic, Encoding.UTF8.GetBytes(message), QoS, true);
        }
        
        /// <summary>
        /// When the broker sends a new message the client enques it to its buffer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            switch(e.Topic)
            {
                case "/temperature":
                    temperatureBuffer.Enqueue(Encoding.UTF8.GetString(e.Message));
                    break;
                case "/humidity":
                    humidityBuffer.Enqueue(Encoding.UTF8.GetString(e.Message));
                    break;
                case "/motion":
                    motionBuffer = Encoding.UTF8.GetString(e.Message) == "1";
                    break;
            }
        }
    }
}

namespace MqttBroker
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Linq;
    using uPLibrary.Networking.M2Mqtt;

    /// <summary>
    /// MQTT Broker class by GnatMQ.
    /// </summary>
    public static class MQTTBroker
    {
        public static IPAddress ownIP;
        public static string ownPort = MqttSettings.MQTT_BROKER_DEFAULT_PORT.ToString();
        static MqttBroker broker;

        /// <summary>
        /// The methods starts a broker and prints the address and port number it is listening on.
        /// </summary>
        public static void StartBroker()
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            ownIP = localIPs.First(x => x.AddressFamily == AddressFamily.InterNetwork);

            if (broker != null)
            {
                Console.WriteLine("The broker is already running.");
                return;
            }

            broker = new MqttBroker();
            try
            {
                broker.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not start the broker. \n Error message: {0}", e.Message);
                return;
            }
            Console.WriteLine("MQTT Broker is listening on:\r\n");
            Console.WriteLine("\t IP:  \t{0}", ownIP.ToString());
            Console.WriteLine("\t Port:\t{0}\r\n", ownPort);
        }

        /// <summary>
        /// The methods closes the broker.
        /// </summary>
        public static void StopBroker()
        {
            try
            {
                broker.Stop();
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not stop the broker. \n Error message: {0}", e.Message);
                return;
            }
            Console.WriteLine("Broker is stopped.\r\n");
        }
    }
}

namespace CoAP
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Sockets;
    using System.Linq;
    using CoAP.Server;
    using CoAP.Server.Resources;
    using CoAP.Net;

    /// <summary>
    /// CoAP server class by CoAP.NET.
    /// </summary>
    public static class CoAPServer
    {
        public static IPAddress ownIP;
        public static string ownPort;
        public static Queue<string> temperatureBuffer;
        public static Queue<string> humidityBuffer;
        public static bool motionBuffer;

        static CoapServer server;

        /// <summary>
        /// Resource class that can recieve the temperature data from the client.
        /// </summary>
        class TemperatureResource : Resource
        {
            /// <summary>
            /// Constructor of the class.
            /// </summary>
            /// <param name="name"></param>
            public TemperatureResource(String name)
                : base(name)
            { }

            /// <summary>
            /// This function recieves the client message and enques it to the servers message buffer.
            /// </summary>
            /// <param name="exchange"></param>
            public override void HandleRequest(Exchange exchange)
            {
                Request request = exchange.Request;
                temperatureBuffer.Enqueue(request.PayloadString.TrimEnd('�'));
            }
        }

        /// <summary>
        /// Resource class that can recieve the humidity data from the client.
        /// </summary>
        class HumidityResource : Resource
        {
            /// <summary>
            /// Constructor of the class.
            /// </summary>
            /// <param name="name"></param>
            public HumidityResource(String name)
                : base(name)
            { }

            /// <summary>
            /// This function recieves the client message and enques it to the servers message buffer.
            /// </summary>
            /// <param name="exchange"></param>
            public override void HandleRequest(Exchange exchange)
            {
                Request request = exchange.Request;
                humidityBuffer.Enqueue(request.PayloadString.TrimEnd('�'));
            }
        }

        /// <summary>
        /// Resource class that can recieve the Motion data from the client.
        /// </summary>
        class MotionResource : Resource
        {
            /// <summary>
            /// Constructor of the class.
            /// </summary>
            /// <param name="name"></param>
            public MotionResource(String name)
                : base(name)
            { }

            /// <summary>
            /// This function recieves the client message and enques it to the servers message buffer.
            /// </summary>
            /// <param name="exchange"></param>
            public override void HandleRequest(Exchange exchange)
            {
                Request request = exchange.Request;
                motionBuffer = request.PayloadString.TrimEnd('�') == "1";
            }
        }

        /// <summary>
        /// The functions starts the CoAP server, adds resource(s) and starts it.
        /// </summary>
        public static void StartServer()
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            ownIP = localIPs.First(x => x.AddressFamily == AddressFamily.InterNetwork);

            temperatureBuffer = new Queue<string>();
            humidityBuffer = new Queue<string>();

            if (server != null)
                return;

            CoapConfig conf = new CoapConfig();
            conf.DefaultPort = 49153;
            conf.DefaultSecurePort = 49153;
            conf.DefaultBlockSize = 1024;
            
            server = new CoapServer(conf);
           
            ownPort = server.Config.DefaultPort.ToString();
            server.Add(new TemperatureResource("Temperature"));
            server.Add(new HumidityResource("Humidity"));
            server.Add(new MotionResource("Motion"));

            try
            {
                server.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not start the server. \n Error message: {0}", e.Message);
                return;
            }
        }

        /// <summary>
        /// The function stops the running CoAP server.
        /// </summary>
        public static void StopServer()
        {
            // TODO: garbage collection
        }
    }
}

namespace Utilities
{
    /// <summary>
    /// Enumerator for the usable communication protocols.
    /// </summary>
    public enum Protocols
    {
        CoAP,
        MQTT
    }
}