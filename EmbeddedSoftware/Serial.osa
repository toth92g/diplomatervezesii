package main;

/* 
 * The provided Serial functions
 */
interface call SerialFunctions
{
	void Init(uint8 UartNumber, uint8 DataLength, uint32 BaudRate);
	void SendCharacter(uint8 character, uint8 UartNumber);
	void SendString(uint8[256] buffer, uint32 length, uint8 UartNumber);
} 

/* 
 * This component handles the UART module
 */
component SerialDriver
{
	/* CoAP interfaces */
	provided SerialFunctions publicFunctions;
	
	/* Memory allocations */
	@MemoryAllocation(policy="absolute", address = "0x400FE000")	// System Control
	SystemControlRegisters SYSCONTROL;
	
	@MemoryAllocation(policy="absolute", address = "0x4000C000") 	// UART0
	UARTRegisters UART0;
	
	@MemoryAllocation(policy="absolute", address = "0x4000D000") 	// UART1
	UARTRegisters UART1;
	
	@MemoryAllocation(policy="absolute", address = "0x4000E000") 	// UART2
	UARTRegisters UART2;
	
	@MemoryAllocation(policy="absolute", address = "0x4000F000") 	// UART3
	UARTRegisters UART3;
	
	@MemoryAllocation(policy="absolute", address = "0x40010000") 	// UART4
	UARTRegisters UART4;
	
	@MemoryAllocation(policy="absolute", address = "0x40011000") 	// UART5
	UARTRegisters UART5;
	
	@MemoryAllocation(policy="absolute", address = "0x40012000") 	// UART6
	UARTRegisters UART6;
	
	@MemoryAllocation(policy="absolute", address = "0x40013000") 	// UART7
	UARTRegisters UART7;
	
	@MemoryAllocation(policy="absolute", address = "0x40004000") 	// GPIO PORT A
	GPIORegisters GPIOA;
	
	@MemoryAllocation(policy="absolute", address = "0x40005000") 	// GPIO PORT B
	GPIORegisters GPIOB;
	
	@MemoryAllocation(policy="absolute", address = "0x40006000") 	// GPIO PORT C
	GPIORegisters GPIOC;
	
	@MemoryAllocation(policy="absolute", address = "0x40061000") 	// GPIO PORT K
	GPIORegisters GPIOK;
	
	@MemoryAllocation(policy="absolute", address = "0x40065000") 	// GPIO PORT P
	GPIORegisters GPIOP;

	/*
	 * Serial module initalization function
	 */
	@Runnable
	@CallTrigger(port="publicFunctions",operation="Init")
	void init(uint8 UartNumber, uint8 DataLength, uint32 BaudRate) 
	{
		uint32 mask_LCRH = (1<<4);		// FIFO enabled
		uint32 mask_IBRD = 0;
		uint32 mask_FBRD = 0;
		
		switch(DataLength)
		{
			case 5:
				mask_LCRH = mask_LCRH | (0x0<<5);
				break;
			case 6:
				mask_LCRH = mask_LCRH | (0x1<<5);
				break;
			case 7:
				mask_LCRH = mask_LCRH | (0x2<<5);
				break;
			case 8:
				mask_LCRH = mask_LCRH | (0x3<<5);
				break;
		}
		
		switch(BaudRate)	// TODO: int<->float conversion needed to calculate the values
		{
			// 120MHz / (16*9600) = 781 + 0.25
			// 0.25 * 64 + 0.5 = 16 + 0.5
			case 9600:
				mask_IBRD = 781;
				mask_FBRD = 16;
				break;
			case 19200:
				mask_IBRD = 390;
				mask_FBRD = 40;
				break;
			case 38400:
				mask_IBRD = 195;
				mask_FBRD = 20;
				break;
			case 57600:
				mask_IBRD = 130;
				mask_FBRD = 14;
				break;
			case 115200:
				mask_IBRD = 65;
				mask_FBRD = 7;
				break;
		}	
		
		SYSCONTROL.RCGCUART = SYSCONTROL.RCGCUART | (1<<UartNumber);	// Enable UARTn
		
		switch(UartNumber)
		{
			case 0:
				SYSCONTROL.RCGCGPIO = SYSCONTROL.RCGCGPIO | (1<<0);	// Enable GPIO Port A (PA0 - U0Rx, PA1 - U0Tx)
				
				GPIOA.AFSEL = GPIOA.AFSEL | (1<<0) | (1<<1);		// PA0 and PA1 will be working as UART pins
				GPIOA.DR2R 	= GPIOA.DR2R | (1<<0) | (1<<1);			// 2-mA drive enable for both PA0 and PA1
				GPIOA.PCTL 	= GPIOA.PCTL | 0x00000011;				// PA0 and PA1 will be the U0Rx and U0Tx signals
				GPIOA.DIR 	= GPIOA.DIR & ~(0x03);					// PA0 and PA1 are inputs
				GPIOA.DEN 	= GPIOA.DEN | (1<<0) | (1<<1);			// Digital functions are enabled for both PA0 and PA1
				 
				UART0.CTL 	= UART0.CTL & ~(0x01);					// Disable UART0
				UART0.IBRD 	= mask_IBRD;
				UART0.FBRD 	= mask_FBRD;
				UART0.LCRH 	= UART0.LCRH | mask_LCRH;
				UART0.CC 	= 0;									// Using the system clock and devide it with the given rate
				UART0.CTL 	= UART0.CTL | (1<<0);					// Enable UART
				break;
			case 1:
				SYSCONTROL.RCGCGPIO = SYSCONTROL.RCGCGPIO | (1<<1);	// Enable GPIO Port B (PB0 - U1Rx, PB1 - U1Tx)
				
				GPIOB.AFSEL = GPIOB.AFSEL | (1<<0) | (1<<1);		// PB0 and PB1 will be working as UART pins
				GPIOB.DR2R 	= GPIOB.DR2R | (1<<0) | (1<<1);			// 2-mA drive enable for both PB0 and PB1
				GPIOB.PCTL 	= GPIOB.PCTL | 0x00000011;				// PB0 and PB1 will be the U1Rx and U1Tx signals
				GPIOB.DIR 	= GPIOB.DIR & ~(0x03);					// PB0 and PB1 are inputs
				GPIOB.DEN 	= GPIOB.DEN | (1<<0) | (1<<1);			// Digital functions are enabled for both PB0 and PB1
				 
				UART1.CTL 	= UART1.CTL & ~(0x01);					// Disable UART
				UART1.IBRD 	= mask_IBRD;
				UART1.FBRD 	= mask_FBRD;
				UART1.LCRH 	= UART1.LCRH | mask_LCRH;
				UART1.CC 	= 0;									// Using the system clock and devide it with the given rate
				UART1.CTL 	= UART1.CTL | (1<<0);					// Enable UART
				break;
			case 2:
				SYSCONTROL.RCGCGPIO = SYSCONTROL.RCGCGPIO | (1<<0);	// Enable GPIO Port A (PA6 - U2Rx, PA7 - U2Tx)
				
				GPIOA.AFSEL = GPIOA.AFSEL | (1<<6) | (1<<7);		// PA6 and PA7 will be working as UART pins
				GPIOA.DR2R 	= GPIOA.DR2R | (1<<6) | (1<<7);			// 2-mA drive enable for both PA6 and PA7
				GPIOA.PCTL 	= GPIOA.PCTL | 0x11000000;				// PA6 and PA7 will be the U2Rx and U2Tx signals
				GPIOA.DIR 	= GPIOA.DIR & ~(0xC0);					// PA6 and PA7 are inputs
				GPIOA.DEN 	= GPIOA.DEN | (1<<6) | (1<<7);			// Digital functions are enabled for both PA6 and PA7
				 
				UART2.CTL 	= UART2.CTL & ~(0x01);					// Disable UART
				UART2.IBRD 	= mask_IBRD;
				UART2.FBRD 	= mask_FBRD;
				UART2.LCRH 	= UART2.LCRH | mask_LCRH;
				UART2.CC 	= 0;									// Using the system clock and devide it with the given rate
				UART2.CTL 	= UART2.CTL | (1<<0);					// Enable UART
				break;
			case 3:
				SYSCONTROL.RCGCGPIO = SYSCONTROL.RCGCGPIO | (1<<0);	// Enable GPIO Port A (PA4 - U3Rx, PA5 - U3Tx)
				
				GPIOA.AFSEL = GPIOA.AFSEL | (1<<4) | (1<<5);		// PA4 and PA5 will be working as UART pins
				GPIOA.DR2R 	= GPIOA.DR2R | (1<<4) | (1<<5);			// 2-mA drive enable for both PA4 and PA5
				GPIOA.PCTL 	= GPIOA.PCTL | 0x00110000;				// PA4 and PA5 will be the U3Rx and U3Tx signals
				GPIOA.DIR 	= GPIOA.DIR & ~(0x30);					// PA4 and PA5 are inputs
				GPIOA.DEN 	= GPIOA.DEN | (1<<4) | (1<<5);			// Digital functions are enabled for both PA4 and PA5
				 
				UART3.CTL 	= UART3.CTL & ~(0x01);					// Disable UART
				UART3.IBRD 	= mask_IBRD;
				UART3.FBRD 	= mask_FBRD;
				UART3.LCRH 	= UART3.LCRH | mask_LCRH;
				UART3.CC 	= 0;									// Using the system clock and devide it with the given rate
				UART3.CTL 	= UART3.CTL | (1<<0);					// Enable UART
				break;
			case 4:
				SYSCONTROL.RCGCGPIO = SYSCONTROL.RCGCGPIO | (1<<9);	// Enable GPIO Port K (PK0 - U4Rx, PK1 - U4Tx)
				
				GPIOK.AFSEL = GPIOK.AFSEL | (1<<0) | (1<<1);		// PK0 and PK1 will be working as UART pins
				GPIOK.DR2R 	= GPIOK.DR2R | (1<<0) | (1<<1);			// 2-mA drive enable for both PK0 and PK1
				GPIOK.PCTL 	= GPIOK.PCTL | 0x00000011;				// PK0 and PK1 will be the U4Rx and U4Tx signals
				GPIOK.DIR 	= GPIOK.DIR & ~(0x03);					// PK0 and PK1 are inputs
				GPIOK.DEN 	= GPIOK.DEN | (1<<0) | (1<<1);			// Digital functions are enabled for both PK0 and PK1
				 
				UART4.CTL 	= UART4.CTL & ~(0x01);					// Disable UART
				UART4.IBRD 	= mask_IBRD;
				UART4.FBRD 	= mask_FBRD;
				UART4.LCRH 	= UART4.LCRH | mask_LCRH;
				UART4.CC 	= 0;									// Using the system clock and devide it with the given rate
				UART4.CTL 	= UART4.CTL | (1<<0);					// Enable UART
				break;
			case 5:
				SYSCONTROL.RCGCGPIO = SYSCONTROL.RCGCGPIO | (1<<2);	// Enable GPIO Port C (PC6 - U5Rx, PC7 - U5Tx)
				
				GPIOC.AFSEL = GPIOC.AFSEL | (1<<6) | (1<<7);		// PC6 and PC7 will be working as UART pins
				GPIOC.DR2R 	= GPIOC.DR2R | (1<<6) | (1<<7);			// 2-mA drive enable for both PC6 and PC7
				GPIOC.PCTL 	= GPIOC.PCTL | 0x11000000;				// PC6 and PC7 will be the U5Rx and U5Tx signals
				GPIOC.DIR 	= GPIOC.DIR & ~(0xC0);					// PC6 and PC7 are inputs
				GPIOC.DEN 	= GPIOC.DEN | (1<<6) | (1<<7);			// Digital functions are enabled for both PC6 and PC7
				 
				UART5.CTL 	= UART5.CTL & ~(0x01);					// Disable UART
				UART5.IBRD 	= mask_IBRD;
				UART5.FBRD 	= mask_FBRD;
				UART5.LCRH 	= UART5.LCRH | mask_LCRH;
				UART5.CC 	= 0;									// Using the system clock and devide it with the given rate
				UART5.CTL 	= UART5.CTL | (1<<0);					// Enable UART
				break;
			case 6:
				SYSCONTROL.RCGCGPIO = SYSCONTROL.RCGCGPIO | (1<<13);// Enable GPIO Port P (PP0 - U6Rx, PP1 - U6Tx)
				
				GPIOP.AFSEL = GPIOP.AFSEL | (1<<0) | (1<<1);		// PP0 and PP1 will be working as UART pins
				GPIOP.DR2R 	= GPIOP.DR2R | (1<<0) | (1<<1);			// 2-mA drive enable for both PP0 and PP1
				GPIOP.PCTL 	= GPIOP.PCTL | 0x00000011;				// PP0 and PP1 will be the U6Rx and U6Tx signals
				GPIOP.DIR 	= GPIOP.DIR & ~(0x03);					// PP0 and PP1 are inputs
				GPIOP.DEN 	= GPIOP.DEN | (1<<0) | (1<<1);			// Digital functions are enabled for both PP0 and PP1
				 
				UART6.CTL 	= UART6.CTL & ~(0x01);					// Disable UART
				UART6.IBRD 	= mask_IBRD;
				UART6.FBRD 	= mask_FBRD;
				UART6.LCRH 	= UART6.LCRH | mask_LCRH;
				UART6.CC 	= 0;									// Using the system clock and devide it with the given rate
				UART6.CTL 	= UART6.CTL | (1<<0);					// Enable UART
				break;
			case 7:
				SYSCONTROL.RCGCGPIO = SYSCONTROL.RCGCGPIO | (1<<2);	// Enable GPIO Port C (PC4 - U7Rx, PC5 - U7Tx)
				
				GPIOC.AFSEL = GPIOC.AFSEL | (1<<4) | (1<<5);		// PC4 and PC5 will be working as UART pins
				GPIOC.DR2R 	= GPIOC.DR2R | (1<<4) | (1<<5);			// 2-mA drive enable for both PC4 and PC5
				GPIOC.PCTL 	= GPIOC.PCTL | 0x00110000;				// PC4 and PC5 will be the U7Rx and U7Tx signals
				GPIOC.DIR 	= GPIOC.DIR & ~(0x30);					// PC4 and PC5 are inputs
				GPIOC.DEN 	= GPIOC.DEN | (1<<4) | (1<<5);			// Digital functions are enabled for both PC4 and PC5
				 
				UART7.CTL 	= UART7.CTL & ~(0x01);					// Disable UART
				UART7.IBRD 	= mask_IBRD;
				UART7.FBRD 	= mask_FBRD;
				UART7.LCRH 	= UART7.LCRH | mask_LCRH;
				UART7.CC 	= 0;									// Using the system clock and devide it with the given rate
				UART7.CTL 	= UART7.CTL | (1<<0);					// Enable UART
				break;
		}
	}

	/* 
	 * Sends the given character via UART
	 */
	@Runnable
	@CallTrigger(port="publicFunctions",operation="SendCharacter")
	void sendCharacter(uint8 character, uint8 UartNumber)
	{
		switch(UartNumber)
		{
			case 0:
				while(UART0.FR & (1<<5)){}	// While transmit fifo is full
				UART0.DR = character;
				break;
			case 1:
				while(UART1.FR & (1<<5)){}	// While transmit fifo is full
				UART1.DR = character;
				break;
			case 2:
				while(UART2.FR & (1<<5)){}	// While transmit fifo is full
				UART2.DR = character;
				break;
			case 3:
				while(UART3.FR & (1<<5)){}	// While transmit fifo is full
				UART3.DR = character;
				break;
			case 4:
				while(UART4.FR & (1<<5)){}	// While transmit fifo is full
				UART4.DR = character;
				break;
			case 5:
				while(UART6.FR & (1<<5)){}	// While transmit fifo is full
				UART5.DR = character;
				break;
			case 6:
				while(UART6.FR & (1<<5)){}	// While transmit fifo is full
				UART6.DR = character;
				break;
			case 7:
				while(UART7.FR & (1<<5)){}	// While transmit fifo is full
				UART7.DR = character;
				break;
		}
	}
	
	/*
	 * Sends the given string via UART
	 */
	@Runnable
	@CallTrigger(port="publicFunctions",operation="SendString")
	void sendString(uint8[256] buffer, uint32 length, uint8 UartNumber)	// TODO characters and strings
	{
		uint32 counter = 0;
		
		sendCharacter(92, UartNumber);   	// Escape character (\)
	    sendCharacter(98, UartNumber);  	// Start marker (b)
		
		for(counter = 0; counter < length; counter = counter + 1)
		{
			sendCharacter(buffer[counter], UartNumber);
	        if(buffer[counter] == 92)         	// Escape character
	            sendCharacter(92, UartNumber);      		// '\' is escape sequence marker so we double it
		}	
	 			
	    sendCharacter(92, UartNumber);  	// Escape character
	    sendCharacter(101, UartNumber);   	// End marker (e)
	}
}
