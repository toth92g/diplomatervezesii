package main;

/* 
 * The provided Hibernate functions
 */
interface call HibernateFunctions
{
	uint8 ReadData(uint8 byteNumber);
	void WriteData(uint8 byteNumber, uint8 dataByte);
}
 
/*
 * This component handles the hibernate functions
 */
component Hibernate
{
	/* Hibernate interfaces */
	provided HibernateFunctions	publicFunctions;
	
	/* Memory allocations */
	@MemoryAllocation(policy="absolute", address = "0x400FE000")	// System Control
	SystemControlRegisters SYSCONTROL;
	
	@MemoryAllocation(policy="absolute", address = "0x400FC000")	// Hibernation Module
	HibernationRegisters HIB;
	
	/*
	 * Waits until the register write is finished, because the clock of this module is not synchronized with the system clock
	 */
	@Runnable
	void writeComplete()
	{
		while(!(HIB.CTL & (1<<31))){}	// Register write is not finished yet
	}
	
	/*
	 * Initializes the module
	 */
	@Runnable
	void Init()
	{
		uint32 tmpCTL = 0; 
		uint32 tmpIC = 0;
		
		SYSCONTROL.RCGCHIB = (1<<0);	// Enable hibernation module
		
		tmpCTL = tmpCTL | (1<<6);		// CLK32EN	- Enable oscillator input
		tmpCTL = tmpCTL | (1<<0);		// RTCEN	- RTC and Calendar mode are enabled
		tmpCTL = tmpCTL | (1<<3);		// RTCWEN	- RTC Match
		tmpCTL = tmpCTL | (1<<4);		// PINWEN	- Wake up pin enable
		
		tmpIC = tmpIC | (1<<0);			// RTCALT0 	- Clear RTC interrupt flag
		tmpIC = tmpIC | (1<<7);			// VDDFAIL	- Clear VDDFAIL flag
		tmpIC = tmpIC | (1<<3);			// EXTW		- External wake-up interrupt clear
		
		HIB.CTL = tmpCTL;
		writeComplete();
		 
		HIB.IC = tmpIC;
		writeComplete(); 
		
		tmpCTL = HIB.CTL;
		tmpCTL = tmpCTL | (1<<8);		// VDD3ON	- The internal switches control the power to the on-chip modules
		tmpCTL = tmpCTL | (1<<30);		// RETCLR	- Retention set
		
		HIB.CTL = tmpCTL;
		writeComplete();
	}

	/*
	 * Sets the time wake up time
	 */
	@Runnable
	void setWakeUpTime(uint32 time)
	{
		HIB.LOCK 	= 0xA3359554;		// Unlock the registers (for example RTCLD)
		writeComplete();
		HIB.RTCLD 	= 0;				// Reset the counter
		writeComplete();
		HIB.LOCK 	= 0;				// Lock the RTCLD again
		writeComplete();
		HIB.RTCM0 	= time;				// Set the time to wait
		writeComplete();
	}
	
	/*
	 * Starts the hibernation cycle
	 */
	@Runnable
	void hibernateRequest()
	{
		HIB.CTL = HIB.CTL | (1<<1);		// HIBREQ	- Hibernation requested
		writeComplete();
	}
	
	/*
	 * This function should be called to set the wakeup time and start the hibernation
	 */
	@Runnable 
	void Hibernate(uint32 time)
	{
		setWakeUpTime(time);
		hibernateRequest();
	}

	/*
	 * Reads the data from the given register byte (1 to 64)
	 */
	@Runnable
	@CallTrigger(port="publicFunctions",operation="ReadData")
	uint8 readData(uint8 byteNumber)
	{
		return (HIB.DATA[byteNumber / 4] >> 8 * (byteNumber % 4));
	}
	
	/*
	 * Writes the given data into the given register byte (1 to 64)
	 */
	@Runnable
	@CallTrigger(port="publicFunctions",operation="WriteData")
	void writeData(uint8 byteNumber, uint8 dataByte)
	{
		uint32 tmpNumber = HIB.DATA[byteNumber / 4];
	    switch(byteNumber % 4)
	    {
	    case 0:
	        tmpNumber = tmpNumber & 0xFFFFFF00;
	        tmpNumber = tmpNumber | dataByte;
	        break;
	    case 1:
	        tmpNumber = tmpNumber & 0xFFFF00FF;
	        tmpNumber = tmpNumber | (dataByte << 8);
	        break;
	    case 2:
	        tmpNumber = tmpNumber & 0xFF00FFFF;
	        tmpNumber = tmpNumber | (dataByte << 16);
	        break;
	    case 3:
	        tmpNumber = tmpNumber & 0x00FFFFFF;
	        tmpNumber = tmpNumber | (dataByte << 24);
	        break;
	    }
	    HIB.DATA[byteNumber / 4] = tmpNumber;
	    writeComplete();
	}
}
