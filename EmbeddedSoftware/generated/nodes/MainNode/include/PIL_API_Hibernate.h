/**
 * OSARES Platform Interface Layer Component API declaration.
 *
 * Component name: Hibernate
 *
 * Generated: 2017-11-23T08:32:38.189+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_API_HIBERNATE_H
#define __PIL_API_HIBERNATE_H
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_API.h> 
 
 
 
 
/* Declaration of the function writeComplete   */
void Hibernate_writeComplete();
/* Declaration of the function Init   */
void Hibernate_Init();
/* Declaration of the function setWakeUpTime   */
void Hibernate_setWakeUpTime(uint32 time);
/* Declaration of the function hibernateRequest   */
void Hibernate_hibernateRequest();
/* Declaration of the function Hibernate   */
void Hibernate_Hibernate(uint32 time);
/* Declaration of the function readData   */
uint8 Hibernate_readData(uint8 byteNumber);
/* Declaration of the function writeData   */
void Hibernate_writeData(uint8 byteNumber,uint8 dataByte);
#endif
