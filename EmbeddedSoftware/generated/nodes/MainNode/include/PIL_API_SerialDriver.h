/**
 * OSARES Platform Interface Layer Component API declaration.
 *
 * Component name: SerialDriver
 *
 * Generated: 2017-11-23T08:32:38.215+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_API_SERIALDRIVER_H
#define __PIL_API_SERIALDRIVER_H
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_API.h> 
 
 
 
 
/* Declaration of the function init   */
void SerialDriver_init(uint8 UartNumber,uint8 DataLength,uint32 BaudRate);
/* Declaration of the function sendCharacter   */
void SerialDriver_sendCharacter(uint8 character,uint8 UartNumber);
/* Declaration of the function sendString   */
void SerialDriver_sendString(uint8 buffer[256],uint32 length,uint8 UartNumber);
#endif
