/**
 * OSARES Platform Interface Layer Application-specific Data Types Declaration
 *
 * Node name: MainNode
 *
 * Generated: 2017-11-23T08:32:38.233+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_APPLICATION_TYPES_H
#define __PIL_APPLICATION_TYPES_H
#include<Osares_BaseTypes.h> 
 
 
/* Declaration of struct data type coapOption */
typedef struct {
	uint8 delta;
	uint8 length;
	uint8 value[32];
} coapOption;

/* Declaration of struct data type coapMessage */
typedef struct {
	uint8 version;
	uint8 type;
	uint8 tokenLength;
	uint8 code;
	uint16 messageId;
	uint8 token;
	uint8 payloadMarker;
	uint8 payload[32];
	uint8 messageLength;
} coapMessage;

/* Declaration of struct data type PhysicalParameters */
typedef struct {
	uint8 Temperature;
	uint8 Humidity;
	uint8 Motion;
} PhysicalParameters;

/* Declaration of struct data type SysTickRegistersType */
typedef struct {
	uint32 CTRL;
	uint32 RELOAD;
	uint32 CURRENT;
} SysTickRegistersType;

/* Declaration of struct data type SystemControlRegisters */
typedef struct {
	uint32 DID0;
	uint32 DID1;
	uint32 DummyBytes1[12];
	uint32 PTBOCTL;
	uint32 DummyBytes2[5];
	uint32 RIS;
	uint32 IMC;
	uint32 MISC;
	uint32 RESC;
	uint32 PWRTC;
	uint32 NMIC;
	uint32 DummyBytes3[5];
	uint32 MOSCCTL;
	uint32 DummyBytes4[12];
	uint32 RSCLKCFG;
	uint32 DummyBytes5[3];
	uint32 MEMTIM0;
	uint32 DummyBytes6[29];
	uint32 ALTCLKCFG;
	uint32 DummyBytes7[2];
	uint32 DSCLKCFG;
	uint32 DIVSCLK;
	uint32 SYSPROP;
	uint32 PIOSCCAL;
	uint32 PIOSCSTAT;
	uint32 DummyBytes8[2];
	uint32 PLLFREQ0;
	uint32 PLLFREQ1;
	uint32 PLLSTAT;
	uint32 DummyBytes9[7];
	uint32 SLPPWRCFG;
	uint32 DSLPPWRCFG;
	uint32 DummyBytes10[4];
	uint32 NVMSTAT;
	uint32 DummyBytes11[4];
	uint32 LDOSPCTL;
	uint32 LDOSPCAL;
	uint32 LDODPCTL;
	uint32 LDODPCAL;
	uint32 DummyBytes12[2];
	uint32 SDPMST;
	uint32 DummyBytes13[2];
	uint32 RESBEHAVCTL;
	uint32 DummyBytes14[6];
	uint32 HSSR;
	uint32 DummyBytes15[34];
	uint32 USBPDS;
	uint32 USBMPC;
	uint32 EMACPDS;
	uint32 EMACMPC;
	uint32 DummyBytes16[2];
	uint32 CAN0PDS;
	uint32 CAN0MPC;
	uint32 CAN1PDS;
	uint32 CAN1MPC;
	uint32 DummyBytes17[22];
	uint32 PPWD;
	uint32 PPTIMER;
	uint32 PPGPIO;
	uint32 PPDMA;
	uint32 PPEPI;
	uint32 PPHIB;
	uint32 PPUART;
	uint32 PPSSI;
	uint32 PPI2C;
	uint32 DummyBytes18;
	uint32 PPUSB;
	uint32 DummyBytes19;
	uint32 PPEPHY;
	uint32 PPCAN;
	uint32 PPADC;
	uint32 PPACMP;
	uint32 PPPWM;
	uint32 PPQEI;
	uint32 PPLPC;
	uint32 DummyBytes20;
	uint32 PPPECI;
	uint32 PPFAN;
	uint32 PPEEPROM;
	uint32 PPWTIMER;
	uint32 DummyBytes21[4];
	uint32 PPRTS;
	uint32 PPCCM;
	uint32 DummyBytes22[6];
	uint32 PPLCD;
	uint32 DummyBytes23;
	uint32 PPOWIRE;
	uint32 PPEMAC;
	uint32 PPPRB;
	uint32 PPHIM;
	uint32 DummyBytes24[86];
	uint32 SRWD;
	uint32 SRTIMER;
	uint32 SRGPIO;
	uint32 SRDMA;
	uint32 SREPI;
	uint32 SRHIB;
	uint32 SRUART;
	uint32 SRSSI;
	uint32 SRI2C;
	uint32 DummyBytes25;
	uint32 SRUSB;
	uint32 DummyBytes26;
	uint32 SREPHY;
	uint32 SRCAN;
	uint32 SRADC;
	uint32 SRACMP;
	uint32 SRPWM;
	uint32 SRQEI;
	uint32 DummyBytes27[4];
	uint32 SREEPROM;
	uint32 DummyBytes28[6];
	uint32 SRCCM;
	uint32 DummyBytes29[9];
	uint32 SREMAC;
	uint32 DummyBytes30[24];
	uint32 RCGCWD;
	uint32 RCGCTIMER;
	uint32 RCGCGPIO;
	uint32 RCGCDMA;
	uint32 RCGCEPI;
	uint32 RCGCHIB;
	uint32 RCGCUART;
	uint32 RCGCSSI;
	uint32 RCGCI2C;
	uint32 DummyBytes31;
	uint32 RCGCUSB;
	uint32 DummyBytes32;
	uint32 RCGCEPHY;
	uint32 RCGCCAN;
	uint32 RCGCADC;
	uint32 RCGCACMP;
	uint32 RCGCPWM;
	uint32 RCGCQEI;
	uint32 DummyBytes33[4];
	uint32 RCGCEEPROM;
	uint32 DummyBytes34[6];
	uint32 RCGCCCM;
	uint32 DummyBytes35[9];
	uint32 RCGCEMAC;
	uint32 DummyBytes36[24];
	uint32 SCGCWD;
	uint32 SCGCTIMER;
	uint32 SCGCGPIO;
	uint32 SCGCDMA;
	uint32 SCGCEPI;
	uint32 SCGCHIB;
	uint32 SCGCUART;
	uint32 SCGCSSI;
	uint32 SCGCI2C;
	uint32 DummyBytes37;
	uint32 SCGCUSB;
	uint32 DummyBytes38;
	uint32 SCGCEPHY;
	uint32 SCGCCAN;
	uint32 SCGCADC;
	uint32 SCGCACMP;
	uint32 SCGCPWM;
	uint32 SCGCQEI;
	uint32 DummyBytes39[4];
	uint32 SCGCEEPROM;
	uint32 DummyBytes40[6];
	uint32 SCGCCCM;
	uint32 DummyBytes41[9];
	uint32 SCGCEMAC;
	uint32 DummyBytes42[24];
	uint32 DCGCWD;
	uint32 DCGCTIMER;
	uint32 DCGCGPIO;
	uint32 DCGCDMA;
	uint32 DCGCEPI;
	uint32 DCGCHIB;
	uint32 DCGCUART;
	uint32 DCGCSSI;
	uint32 DCGCI2C;
	uint32 DummyBytes43;
	uint32 DCGCUSB;
	uint32 DummyBytes44;
	uint32 DCGCEPHY;
	uint32 DCGCCAN;
	uint32 DCGCADC;
	uint32 DCGCACMP;
	uint32 DCGCPWM;
	uint32 DCGCQEI;
	uint32 DummyBytes45[4];
	uint32 DCGCEEPROM;
	uint32 DummyBytes46[6];
	uint32 DCGCCCM;
	uint32 DummyBytes47[9];
	uint32 DCGCEMAC;
	uint32 DummyBytes48[24];
	uint32 PCWD;
	uint32 PCTIMER;
	uint32 PCGPIO;
	uint32 PCDMA;
	uint32 PCEPI;
	uint32 PCHIB;
	uint32 PCUART;
	uint32 PCSSI;
	uint32 PCI2C;
	uint32 DummyBytes49;
	uint32 PCUSB;
	uint32 DummyBytes50;
	uint32 PCEPHY;
	uint32 PCCAN;
	uint32 PCADC;
	uint32 PCACMP;
	uint32 PCPWM;
	uint32 PCQEI;
	uint32 DummyBytes51[4];
	uint32 PCEEPROM;
	uint32 DummyBytes52[6];
	uint32 PCCCM;
	uint32 DummyBytes53[9];
	uint32 PCEMAC;
	uint32 DummyBytes54[24];
	uint32 PRWD;
	uint32 PRTIMER;
	uint32 PRGPIO;
	uint32 PRDMA;
	uint32 PREPI;
	uint32 PRHIB;
	uint32 PRUART;
	uint32 PRSSI;
	uint32 PRI2C;
	uint32 DummyBytes55;
	uint32 PRUSB;
	uint32 DummyBytes56;
	uint32 PREPHY;
	uint32 PRCAN;
	uint32 PRADC;
	uint32 PRACMP;
	uint32 PRPWM;
	uint32 PRQEI;
	uint32 DummyBytes57[4];
	uint32 PREEPROM;
	uint32 DummyBytes58[6];
	uint32 PRCCM;
	uint32 DummyBytes59[9];
	uint32 PREMAC;
	uint32 DummyBytes60[288];
	uint32 UNIQUEID0;
	uint32 UNIQUEID1;
	uint32 UNIQUEID2;
	uint32 UNIQUEID3;
} SystemControlRegisters;

/* Declaration of struct data type GPIORegisters */
typedef struct {
	uint32 DATA[256];
	uint32 DIR;
	uint32 IS;
	uint32 RBE;
	uint32 IEV;
	uint32 IM;
	uint32 RIS;
	uint32 MIS;
	uint32 ICR;
	uint32 AFSEL;
	uint32 DummyBytes1[55];
	uint32 DR2R;
	uint32 DR4R;
	uint32 DR8R;
	uint32 ODR;
	uint32 PUR;
	uint32 PDR;
	uint32 SLR;
	uint32 DEN;
	uint32 LOCK;
	uint32 CR;
	uint32 AMSEL;
	uint32 PCTL;
	uint32 ADCCTL;
	uint32 DMACTL;
	uint32 SI;
	uint32 DR12R;
	uint32 WAKEPEN;
	uint32 WAKELVL;
	uint32 WAKESTAT;
	uint32 DummyBytes2[669];
	uint32 PP;
	uint32 PC;
	uint32 DummyBytes3[2];
	uint32 PeriphID4;
	uint32 PeriphID5;
	uint32 PeriphID6;
	uint32 PeriphID7;
	uint32 PeriphID0;
	uint32 PeriphID1;
	uint32 PeriphID2;
	uint32 PeriphID3;
	uint32 PCellID0;
	uint32 PCellID1;
	uint32 PCellID2;
	uint32 PCellID3;
} GPIORegisters;

/* Declaration of struct data type UARTRegisters */
typedef struct {
	uint32 DR;
	uint32 RSR_ECR;
	uint32 DummyBytes1[4];
	uint32 FR;
	uint32 DummyBytes2;
	uint32 ILPR;
	uint32 IBRD;
	uint32 FBRD;
	uint32 LCRH;
	uint32 CTL;
	uint32 IFLS;
	uint32 IM;
	uint32 RIS;
	uint32 MIS;
	uint32 ICR;
	uint32 DMACTL;
	uint32 DummyBytes3[22];
	uint32 ADDR;
	uint32 AMASK;
	uint32 DummyBytes4[965];
	uint32 PP;
	uint32 DummyBytes5;
	uint32 CC;
	uint32 DummyBytes6;
	uint32 PeriphID4;
	uint32 PeriphID5;
	uint32 PeriphID6;
	uint32 PeriphID7;
	uint32 PeriphID0;
	uint32 PeriphID1;
	uint32 PeriphID2;
	uint32 PeriphID3;
	uint32 PCellID0;
	uint32 PCellID1;
	uint32 PCellID2;
	uint32 PCellID3;
} UARTRegisters;

/* Declaration of struct data type ADCRegisters */
typedef struct {
	uint32 ACTSS;
	uint32 RIS;
	uint32 IM;
	uint32 ISC;
	uint32 OSTAT;
	uint32 EMUX;
	uint32 USTAT;
	uint32 TSSEL;
	uint32 SSPRI;
	uint32 SPC;
	uint32 PSSI;
	uint32 DummyBytes;
	uint32 SAC;
	uint32 DCISC;
	uint32 CTL;
	uint32 DummyBytes2;
	uint32 SSMUX0;
	uint32 SSCTL0;
	uint32 SSFIFO0;
	uint32 SSFSTAT0;
	uint32 SSOP0;
	uint32 SSDC0;
	uint32 SSEMUX0;
	uint32 SSTSH0;
	uint32 SSMUX1;
	uint32 SSCTL1;
	uint32 SSFIFO1;
	uint32 SSFSTAT1;
	uint32 SSOP1;
	uint32 SSDC1;
	uint32 SSEMUX1;
	uint32 SSTSH1;
	uint32 SSMUX2;
	uint32 SSCTL2;
	uint32 SSFIFO2;
	uint32 SSFSTAT2;
	uint32 SSOP2;
	uint32 SSDC2;
	uint32 SSEMUX2;
	uint32 SSTSH2;
	uint32 SSMUX3;
	uint32 SSCTL3;
	uint32 SSFIFO3;
	uint32 SSFSTAT3;
	uint32 SSOP3;
	uint32 SSDC3;
	uint32 SSEMUX3;
	uint32 SSTSH3;
	uint32 DummyBytes3[784];
	uint32 DCRIC;
	uint32 DummyBytes4[63];
	uint32 DCCTL0;
	uint32 DCCTL1;
	uint32 DCCTL2;
	uint32 DCCTL3;
	uint32 DCCTL4;
	uint32 DCCTL5;
	uint32 DCCTL6;
	uint32 DCCTL7;
	uint32 DummyBytes5[8];
	uint32 DCCMP0;
	uint32 DCCMP1;
	uint32 DCCMP2;
	uint32 DCCMP3;
	uint32 DCCMP4;
	uint32 DCCMP5;
	uint32 DCCMP6;
	uint32 DCCMP7;
	uint32 DummyBytes6[88];
	uint32 PP;
	uint32 PC;
	uint32 CC;
} ADCRegisters;

/* Declaration of struct data type HibernationRegisters */
typedef struct {
	uint32 RTCC;
	uint32 RTCM0;
	uint32 DummyBytes1;
	uint32 RTCLD;
	uint32 CTL;
	uint32 IM;
	uint32 RIS;
	uint32 MIS;
	uint32 IC;
	uint32 RTCT;
	uint32 RTCSS;
	uint32 IO;
	uint32 DATA[16];
	uint32 DummyBytes2[164];
	uint32 CALCTL;
	uint32 DummyBytes3[3];
	uint32 CAL0;
	uint32 CAL1;
	uint32 DummyBytes4[2];
	uint32 CALLD0;
	uint32 CALLD1;
	uint32 DummyBytes5[2];
	uint32 CALM0;
	uint32 CALM1;
	uint32 DummyBytes6[10];
	uint32 LOCK;
	uint32 DummyBytes7[39];
	uint32 TPCTL;
	uint32 TPSTAT;
	uint32 DummyBytes8[2];
	uint32 TPIO;
	uint32 DummyBytes9[51];
	uint32 TPLOG0;
	uint32 TPLOG1;
	uint32 TPLOG2;
	uint32 TPLOG3;
	uint32 TPLOG4;
	uint32 TPLOG5;
	uint32 TPLOG6;
	uint32 TPLOG7;
	uint32 DummyBytes10[688];
	uint32 PP;
	uint32 DummyBytes11;
	uint32 CC;
} HibernationRegisters;

#endif
