/**
 * OSARES Platform Interface Layer Component API declaration.
 *
 * Component name: TivaTicker
 *
 * Generated: 2017-11-23T08:32:38.195+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_API_TIVATICKER_H
#define __PIL_API_TIVATICKER_H
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_API.h> 
 
 
 
 
/* Declaration of the function initTicker   */
void TivaTicker_initTicker(uint32 periodUs);
/* Declaration of the function waitForSysTick   */
void TivaTicker_waitForSysTick();
#endif
