/**
 * OSARES Platform Interface Layer Component API declaration.
 *
 * Component name: SystemClock
 *
 * Generated: 2017-11-23T08:32:38.178+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_API_SYSTEMCLOCK_H
#define __PIL_API_SYSTEMCLOCK_H
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_API.h> 
 
 
 
 
/* Declaration of the function Init   */
uint32 SystemClock_Init();
#endif
