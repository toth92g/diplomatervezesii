/**
 * OSARES Platform Interface Layer Component API declaration.
 *
 * Component name: GpioDriver
 *
 * Generated: 2017-11-23T08:32:38.172+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_API_GPIODRIVER_H
#define __PIL_API_GPIODRIVER_H
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_API.h> 
 
 
 
 
/* Declaration of the function init   */
void GpioDriver_init(uint8 port,uint8 pin,uint8 isOutput,uint8 isDigital);
/* Declaration of the function set   */
void GpioDriver_set(uint8 port,uint8 pin);
/* Declaration of the function clear   */
void GpioDriver_clear(uint8 port,uint8 pin);
/* Declaration of the function toggle   */
void GpioDriver_toggle(uint8 port,uint8 pin);
/* Declaration of the function read   */
uint8 GpioDriver_read(uint8 port,uint8 pin);
#endif
