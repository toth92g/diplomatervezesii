/**
 * OSARES Platform Interface Layer Component API declaration.
 *
 * Component name: AdcDriver
 *
 * Generated: 2017-11-23T08:32:38.201+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_API_ADCDRIVER_H
#define __PIL_API_ADCDRIVER_H
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_API.h> 
 
 
#define  PIL_Write_AdcDriver_publicDatas_FIFOs(e)	{					{\
						uint32 __tmpi0;\
						for (__tmpi0=0;__tmpi0<8;__tmpi0++)\
								PIL_DataBuffer_COMM_adcDatas_FIFOs[__tmpi0]=e[__tmpi0];\
					}\
;}
#define  PIL_Write_AdcDriver_publicDatas_asd(e)	{						PIL_DataBuffer_COMM_adcDatas_asd=e;\
;}
 
 
/* Declaration of the function EnableSequencer   */
void AdcDriver_EnableSequencer(ADCRegisters* ADC,uint8 Sequencer);
/* Declaration of the function DisableSequencer   */
void AdcDriver_DisableSequencer(ADCRegisters* ADC,uint8 Sequencer);
/* Declaration of the function SequencerInputConfiguration   */
void AdcDriver_SequencerInputConfiguration(ADCRegisters* ADC,uint8 Sequencer,uint8 Input0,uint8 Input1,uint8 Input2,uint8 Input3,uint8 Input4,uint8 Input5,uint8 Input6,uint8 Input7);
/* Declaration of the function EnableSequencerInterrupt   */
void AdcDriver_EnableSequencerInterrupt(ADCRegisters* ADC,uint8 Sequencer);
/* Declaration of the function DisableSequencerInterrupt   */
void AdcDriver_DisableSequencerInterrupt(ADCRegisters* ADC,uint8 Sequencer);
/* Declaration of the function SetTiming   */
void AdcDriver_SetTiming(ADCRegisters* ADC,uint32 ClockDivisionRate);
/* Declaration of the function ConfigGpios   */
void AdcDriver_ConfigGpios(GPIORegisters* GPIO,uint8 gpio,uint8 pin);
/* Declaration of the function ConfigInputPin   */
void AdcDriver_ConfigInputPin(uint8 input);
/* Declaration of the function SampleAveraging   */
void AdcDriver_SampleAveraging(ADCRegisters* ADC,uint8 overSamplingPower);
/* Declaration of the function init   */
void AdcDriver_init(uint8 AdcNumber,uint8 Sequencer,uint32 ClockDivisionRate,uint8 HardwareAveraging,uint8 Input0,uint8 Input1,uint8 Input2,uint8 Input3,uint8 Input4,uint8 Input5,uint8 Input6,uint8 Input7);
/* Declaration of the function measurementHelper   */
void AdcDriver_measurementHelper(ADCRegisters* ADC,uint8 Sequencer);
/* Declaration of the function measure   */
void AdcDriver_measure(uint8 AdcNumber,uint8 Sequencer);
#endif
