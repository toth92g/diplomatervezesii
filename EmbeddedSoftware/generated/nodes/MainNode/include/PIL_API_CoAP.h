/**
 * OSARES Platform Interface Layer Component API declaration.
 *
 * Component name: CoAP
 *
 * Generated: 2017-11-23T08:32:38.208+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_API_COAP_H
#define __PIL_API_COAP_H
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_API.h> 
 
 
#define  PIL_Write_CoAP_publicData_Serialized(e)	{					{\
						uint32 __tmpi0;\
						for (__tmpi0=0;__tmpi0<100;__tmpi0++)\
								PIL_DataBuffer_COMM_coapMessage_Serialized[__tmpi0]=e[__tmpi0];\
					}\
;}
#define  PIL_Write_CoAP_publicData_Length(e)	{						PIL_DataBuffer_COMM_coapMessage_Length=e;\
;}
 
 
/* Declaration of the function CreateOption   */
void CoAP_CreateOption(uint8 delta,uint8 resourceLength,uint8 resource[32]);
/* Declaration of the function CreateMessage   */
void CoAP_CreateMessage(uint8 type,uint8 tokenLength,uint8 code,uint16 messageId,uint8 payloadLength,uint8 payload[4],uint8 optionDelta,uint8 optionResourceLength,uint8 optionResource[32]);
/* Declaration of the function SerializeMessage   */
void CoAP_SerializeMessage();
#endif
