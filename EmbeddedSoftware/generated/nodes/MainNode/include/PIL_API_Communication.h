/**
 * OSARES Platform Interface Layer Component API declaration.
 *
 * Component name: Communication
 *
 * Generated: 2017-11-23T08:32:38.183+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_API_COMMUNICATION_H
#define __PIL_API_COMMUNICATION_H
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_API.h> 
 
 
#define  PIL_Call_Communication_gpioFunctions_Init(a, b, c, d)	GpioDriver_init(a, b, c, d)
#define  PIL_Call_Communication_gpioFunctions_Set(a, b)	GpioDriver_set(a, b)
#define  PIL_Call_Communication_gpioFunctions_Clear(a, b)	GpioDriver_clear(a, b)
#define  PIL_Call_Communication_gpioFunctions_Toggle(a, b)	GpioDriver_toggle(a, b)
#define  PIL_Call_Communication_gpioFunctions_Read(a, b)	GpioDriver_read(a, b)
#define  PIL_Call_Communication_hibernateFunctions_ReadData(a)	Hibernate_readData(a)
#define  PIL_Call_Communication_hibernateFunctions_WriteData(a, b)	Hibernate_writeData(a, b)
#define  PIL_Call_Communication_coapFunctions_coapCreateMessage(a, b, c, d, e, f, g, h, i)	CoAP_CreateMessage(a, b, c, d, e, f, g, h, i)
#define  PIL_Call_Communication_coapFunctions_coapSerializeMessage()	CoAP_SerializeMessage()
#define  PIL_Read_Communication_coapMessage_Serialized()	PIL_DataBuffer_COMM_coapMessage_Serialized
#define  PIL_Read_Communication_coapMessage_Length()	PIL_DataBuffer_COMM_coapMessage_Length
#define  PIL_Call_Communication_adcFunctions_Init(a, b, c, d, e, f, g, h, i, j, k)	AdcDriver_init(a, b, c, d, e, f, g, h, i, j, k)
#define  PIL_Call_Communication_adcFunctions_Measure(a, b)	AdcDriver_measure(a, b)
#define  PIL_Read_Communication_adcDatas_FIFOs()	PIL_DataBuffer_COMM_adcDatas_FIFOs
#define  PIL_Read_Communication_adcDatas_asd()	PIL_DataBuffer_COMM_adcDatas_asd
#define  PIL_Call_Communication_serialFunctions_Init(a, b, c)	SerialDriver_init(a, b, c)
#define  PIL_Call_Communication_serialFunctions_SendCharacter(a, b)	SerialDriver_sendCharacter(a, b)
#define  PIL_Call_Communication_serialFunctions_SendString(a, b, c)	SerialDriver_sendString(a, b, c)
 
 
/* Declaration of the function initArray   */
void Communication_initArray();
/* Declaration of the function calculateVoltageMatrix   */
void Communication_calculateVoltageMatrix();
/* Declaration of the function linearInterpolation   */
float32 Communication_linearInterpolation(float32 number1,float32 number2,float32 factor);
/* Declaration of the function calculateHumidity   */
float32 Communication_calculateHumidity(uint8 temperature,float32 measuredVoltage);
/* Declaration of the function calculateSensorValues   */
void Communication_calculateSensorValues(uint8* temperature,uint8* humidity,uint8* motion);
/* Declaration of the function itoa   */
void Communication_itoa(uint8 value,uint8 valueBuffer[4]);
/* Declaration of the function numberOfDigits   */
uint8 Communication_numberOfDigits(uint16 number,uint8 scale);
/* Declaration of the function sendMessageSerial   */
void Communication_sendMessageSerial(uint8 UartNumber,uint8 value,uint16 messageId,uint8 resource[32],uint8 resourceLength);
/* Declaration of the function GetPhysicalValues   */
void Communication_GetPhysicalValues(uint8 Adc,uint8 Sequencer);
/* Declaration of the function timeoutHandling   */
void Communication_timeoutHandling();
/* Declaration of the function SendPhysicalValues   */
uint8 Communication_SendPhysicalValues(uint8 UartNumber);
#endif
