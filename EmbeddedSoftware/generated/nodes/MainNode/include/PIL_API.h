/**
 * OSARES Platform Interface Layer API Declaration
 *
 * Node name: MainNode
 *
 * Generated: 2017-11-23T08:32:38.221+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_API_H
#define __PIL_API_H
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
 
 
/* PIL Data Buffers */ 
extern uint8 PIL_DataBuffer_COMM_coapMessage_Serialized[100];
extern uint16 PIL_DataBuffer_COMM_coapMessage_Length;
extern uint16 PIL_DataBuffer_COMM_adcDatas_FIFOs[8];
extern uint8 PIL_DataBuffer_COMM_adcDatas_asd;
#endif
