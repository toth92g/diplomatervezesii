#include <Osares_BaseTypes.h>

//*****************************************************************************
//
// Forward declaration of the default fault handlers.
//
//*****************************************************************************
void ResetISR(void);
static void IntDefaultHandler(void);


#ifndef HWREG
#define HWREG(x) (*((volatile uint32 *)(x)))
#endif

//*****************************************************************************
//
// The entry point for the application.
//
//*****************************************************************************
extern int main(void);

//*****************************************************************************
//
// Reserve space for the system stack.
//
//*****************************************************************************
static uint32 pui32Stack[128];

//*****************************************************************************
//
// External declarations for the interrupt handlers used by the application.
//
//*****************************************************************************


__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) =
{
    (void (*)(void))((uint32)pui32Stack + sizeof(pui32Stack)),
                                            // The initial stack pointer
    ResetISR,                               // The reset handler
    IntDefaultHandler,                               // NMIHandler
    IntDefaultHandler,                               // HardFaultHandler
    IntDefaultHandler,                               // MPUFaultHandler
    IntDefaultHandler,                               // BusFaultHandler
    IntDefaultHandler,                               // UsageFaultHandler
    IntDefaultHandler,                               // 
    IntDefaultHandler,                               // 
    IntDefaultHandler,                               // 
    IntDefaultHandler,                               // 
    IntDefaultHandler,                               // SVCallHandler
    IntDefaultHandler,                               // DebugMonitorHandler
    IntDefaultHandler,                               // 
    IntDefaultHandler,                               // PendSVHandler
    IntDefaultHandler,                               // SysTickHandler
    IntDefaultHandler,                               // GPIOPortA
    IntDefaultHandler,                               // GPIOPortB
    IntDefaultHandler,                               // GPIOPortC
    IntDefaultHandler,                               // GPIOPortD
    IntDefaultHandler,                               // GPIOPortE
    IntDefaultHandler,                               // UART0RxTx
    IntDefaultHandler,                               // UART1RxTx
    IntDefaultHandler,                               // SSI0RxTx
    IntDefaultHandler,                               // I2C0MasterSlave
    IntDefaultHandler,                               // PWMFault
    IntDefaultHandler,                               // PWMGenerator0
    IntDefaultHandler,                               // PWMGenerator1
    IntDefaultHandler,                               // PWMGenerator2
    IntDefaultHandler,                               // QuadratureEncoder0
    IntDefaultHandler,                               // ADCSequence0
    IntDefaultHandler,                               // ADCSequence1
    IntDefaultHandler,                               // ADCSequence2
    IntDefaultHandler,                               // ADCSequence3
    IntDefaultHandler,                               // WatchdogTimer
    IntDefaultHandler,                               // Timer0SubA
    IntDefaultHandler,                               // Timer0SubB
    IntDefaultHandler,                               // Timer1SubA
    IntDefaultHandler,                               // Timer1SubB
    IntDefaultHandler,                               // Timer2SubA
    IntDefaultHandler,                               // Timer2SubB
    IntDefaultHandler,                               // AnalogComparator0
    IntDefaultHandler,                               // AnalogComparator1
    IntDefaultHandler,                               // AnalogComparator2
    IntDefaultHandler,                               // SystemControl
    IntDefaultHandler,                               // FLASHControl
    IntDefaultHandler,                               // GPIOPortF
    IntDefaultHandler,                               // GPIOPortG
    IntDefaultHandler,                               // GPIOPortH
    IntDefaultHandler,                               // UART2RxTx
    IntDefaultHandler,                               // SSI1RxTx
    IntDefaultHandler,                               // Timer3SubA
    IntDefaultHandler,                               // Timer3SubB
    IntDefaultHandler,                               // I2C1MasterSlave
    IntDefaultHandler,                               // CAN0
    IntDefaultHandler,                               // CAN1
    IntDefaultHandler,                               // Ethernet
    IntDefaultHandler,                               // Hibernate
    IntDefaultHandler,                               // USB0
    IntDefaultHandler,                               // PWMGenerator3
    IntDefaultHandler,                               // uDMASoftwareTransfer
    IntDefaultHandler,                               // uDMAError
    IntDefaultHandler,                               // ADC1Sequence0
    IntDefaultHandler,                               // ADC1Sequence1
    IntDefaultHandler,                               // ADC1Sequence2
    IntDefaultHandler,                               // ADC1Sequence3
    IntDefaultHandler,                               // ExternalBusInterface0
    IntDefaultHandler,                               // GPIOPortJ
    IntDefaultHandler,                               // GPIOPortK
    IntDefaultHandler,                               // GPIOPortL
    IntDefaultHandler,                               // SSI2RxTx
    IntDefaultHandler,                               // SSI3RxTx
    IntDefaultHandler,                               // UART3RxTx
    IntDefaultHandler,                               // UART4RxTx
    IntDefaultHandler,                               // UART5RxTx
    IntDefaultHandler,                               // UART6RxTx
    IntDefaultHandler,                               // UART7RxTx
    IntDefaultHandler,                               // I2C2MasterSlave
    IntDefaultHandler,                               // I2C3MasterSlave
    IntDefaultHandler,                               // Timer4SubA
    IntDefaultHandler,                               // Timer4SubB
    IntDefaultHandler,                               // Timer5SubA
    IntDefaultHandler,                               // Timer5SubB
    IntDefaultHandler,                               // FPU
    IntDefaultHandler,                               // 
    IntDefaultHandler,                               // 
    IntDefaultHandler,                               // I2C4MasterSlave
    IntDefaultHandler,                               // I2C5MasterSlave
    IntDefaultHandler,                               // GPIOPortM
    IntDefaultHandler,                               // GPIOPortN
    IntDefaultHandler,                               // 
    IntDefaultHandler,                               // Tamper
    IntDefaultHandler,                               // GPIOPortP
    IntDefaultHandler,                               // GPIOPortP1
    IntDefaultHandler,                               // GPIOPortP2
    IntDefaultHandler,                               // GPIOPortP3
    IntDefaultHandler,                               // GPIOPortP4
    IntDefaultHandler,                               // GPIOPortP5
    IntDefaultHandler,                               // GPIOPortP6
    IntDefaultHandler,                               // GPIOPortP7
    IntDefaultHandler,                               // GPIOPortQ
    IntDefaultHandler,                               // GPIOPortQ1
    IntDefaultHandler,                               // GPIOPortQ2
    IntDefaultHandler,                               // GPIOPortQ3
    IntDefaultHandler,                               // GPIOPortQ4
    IntDefaultHandler,                               // GPIOPortQ5
    IntDefaultHandler,                               // GPIOPortQ6
    IntDefaultHandler,                               // GPIOPortQ7
    IntDefaultHandler,                               // GPIOPortR
    IntDefaultHandler,                               // GPIOPortS
    IntDefaultHandler,                               // SHAMD50
    IntDefaultHandler,                               // AES0
    IntDefaultHandler,                               // DES3DES0
    IntDefaultHandler,                               // LCDController0
    IntDefaultHandler,                               // Timer6SubA
    IntDefaultHandler,                               // Timer6SubB
    IntDefaultHandler,                               // Timer7SubA
    IntDefaultHandler,                               // Timer7SubB
    IntDefaultHandler,                               // I2C6MasterSlave
    IntDefaultHandler,                               // I2C7MasterSlave
    IntDefaultHandler,                               // HIMScanMatrixKeyboard0
    IntDefaultHandler,                               // OneWire0
    IntDefaultHandler,                               // HIMPS20
    IntDefaultHandler,                               // HIMLEDSequencer0
    IntDefaultHandler,                               // HIMConsumeIR0
    IntDefaultHandler,                               // I2C8MasterSlave
    IntDefaultHandler,                               // I2C9MasterSlave
    IntDefaultHandler,                               // GPIOPortT
    IntDefaultHandler,                               // Fan1
    IntDefaultHandler,                               // 
};
//*****************************************************************************
//
// The following are constructs created by the linker, indicating where the
// the "data" and "bss" segments reside in memory.  The initializers for the
// for the "data" segment resides immediately following the "text" segment.
//
//*****************************************************************************
extern uint32 __etext;
extern uint32 __data_start__;
extern uint32 __data_end__;
extern uint32 __bss_start__;
extern uint32 __bss_end__;

//*****************************************************************************
//
// This is the code that gets called when the processor first starts execution
// following a reset event.  Only the absolutely necessary set is performed,
// after which the application supplied entry() routine is called.  Any fancy
// actions (such as making decisions based on the reset cause register, and
// resetting the bits in that register) are left solely in the hands of the
// application.
//
//*****************************************************************************
void ResetISR(void)
{
    uint32 *pui32Src, *pui32Dest;
    
    //
    // Copy the data segment initializers from flash to SRAM.
    //
    pui32Src = &__etext;
    for(pui32Dest = &__data_start__; pui32Dest < &__data_end__; )
    {
        *pui32Dest++ = *pui32Src++;
    }
    
    //
    // Zero fill the bss segment.
    //
    __asm("    ldr     r0, =__bss_start__\n"
          "    ldr     r1, =__bss_end__\n"
          "    mov     r2, #0\n"
          "    .thumb_func\n"
          "zero_loop:\n"
          "        cmp     r0, r1\n"
          "        it      lt\n"
          "        strlt   r2, [r0], #4\n"
          "        blt     zero_loop");
    
    //
    // Enable the floating-point unit.  This must be done here to handle the
    // case where main() uses floating-point and the function prologue saves
    // floating-point registers (which will fault if floating-point is not
    // enabled).  Any configuration of the floating-point unit using DriverLib
    // APIs must be done here prior to the floating-point unit being enabled.
    //
    // Note that this does not use DriverLib since it might not be included in
    // this project.
    //
    HWREG(0xE000ED88) = ((HWREG(0xE000ED88) & ~0x00F00000) | 0x00F00000);
    
    //
    // Call the application's entry point.
    //
    main();
}

//*****************************************************************************
//
// This is the code that gets called when the processor receives an unexpected
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void IntDefaultHandler(void)
{
    //
    // Go into an infinite loop.
    //
    while(1)
    {
    }
}
