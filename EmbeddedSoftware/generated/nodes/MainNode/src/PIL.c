/**
 * OSARES Platform Interface Layer Implementation.
 *
 * Node name: MainNode
 *
 * Generated: 2017-11-23T08:32:38.227+01:00[Europe/Prague]
 *
 */
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_API_GpioDriver.h>
#include<PIL_API_SystemClock.h>
#include<PIL_API_Communication.h>
#include<PIL_API_Hibernate.h>
#include<PIL_API_TivaTicker.h>
#include<PIL_API_AdcDriver.h>
#include<PIL_API_CoAP.h>
#include<PIL_API_SerialDriver.h>
 
 
/* PIL Data Buffers */ 
uint8 PIL_DataBuffer_COMM_coapMessage_Serialized[100];
uint16 PIL_DataBuffer_COMM_coapMessage_Length;
uint16 PIL_DataBuffer_COMM_adcDatas_FIFOs[8];
uint8 PIL_DataBuffer_COMM_adcDatas_asd;
 
 
/* Implementation of the function init   */
void Node_MainNode_init()
{
	uint32 i = 0;
	SystemClock_Init();
	GpioDriver_init(3, 2, 1, 1);
	GpioDriver_init(1, 5, 1, 1);
	GpioDriver_init(1, 4, 0, 1);
	SerialDriver_init(6, 8, 9600);
	AdcDriver_init(0, 1, 30, 6, 3, 15, 9, 14, 0, 0, 0, 0);
	Hibernate_Init();
	Communication_GetPhysicalValues(0, 1);
	Communication_SendPhysicalValues(6);
	for(i = 0;i < 200000;i = i + 1)
		{
		}
	Hibernate_Hibernate(30);
}
 
 
/* Implementation of the function DummyTask   */
void Node_MainNode_DummyTask()
{
}
 
 
int main()
{
	uint32 taskExecutionCounter=0;
	/* Init scheduler */
	TivaTicker_initTicker(1000000);

	/* Perform initialization */
	Node_MainNode_init();

	while(1)
	{
		Node_MainNode_DummyTask();

		/* Wait for scheduler tick. */
		TivaTicker_waitForSysTick();
		taskExecutionCounter = (taskExecutionCounter + 1) % 1;
	}
}
