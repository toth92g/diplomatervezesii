/**
 * OSARES Platform Interface Layer Global Constants Declaration
 *
 * Node name: MainNode
 *
 * Generated: 2017-11-23T08:32:38.251+01:00[Europe/Prague]
 *
 */
 
#ifndef __PIL_GLOBAL_CONSTANTS_H
#define __PIL_GLOBAL_CONSTANTS_H
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_GlobalConstants.h> 
 
 
#endif
