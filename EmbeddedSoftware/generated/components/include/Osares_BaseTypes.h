/* Base types... */

#ifndef __OSARES_BASE_TYPES_H
#define __OSARES_BASE_TYPES_H

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;

typedef signed char sint8;
typedef short sint16;
typedef int sint32;

typedef float float32;


#endif