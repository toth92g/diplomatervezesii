/**
 * OSARES Component low level implementation.
 *
 * Component name: TivaTicker
 *
 * Generated: 2017-11-23T08:32:38.123+01:00[Europe/Prague]
 *
 */
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_GlobalConstants.h> 
#include<PIL_API_TivaTicker.h> 
 
 
/* Implementation of the member variable systickRegs   */
static SysTickRegistersType* systickRegs = ((SysTickRegistersType*)0xE000E010);
 
 
/* Implementation of the function initTicker   */
void TivaTicker_initTicker(uint32 periodUs)
{
	(*systickRegs).RELOAD = 4 * periodUs;
	(*systickRegs).CTRL = (1 << 0);
}
/* Implementation of the function waitForSysTick   */
void TivaTicker_waitForSysTick()
{
	while(((*systickRegs).CTRL & (1 << 16)) == 0)
		;
}
 
 
 
 
