/**
 * OSARES Component low level implementation.
 *
 * Component name: SerialDriver
 *
 * Generated: 2017-11-23T08:32:38.155+01:00[Europe/Prague]
 *
 */
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_GlobalConstants.h> 
#include<PIL_API_SerialDriver.h> 
 
 
/* Implementation of the member variable SYSCONTROL   */
static SystemControlRegisters* SYSCONTROL = ((SystemControlRegisters*)0x400FE000);
/* Implementation of the member variable UART0   */
static UARTRegisters* UART0 = ((UARTRegisters*)0x4000C000);
/* Implementation of the member variable UART1   */
static UARTRegisters* UART1 = ((UARTRegisters*)0x4000D000);
/* Implementation of the member variable UART2   */
static UARTRegisters* UART2 = ((UARTRegisters*)0x4000E000);
/* Implementation of the member variable UART3   */
static UARTRegisters* UART3 = ((UARTRegisters*)0x4000F000);
/* Implementation of the member variable UART4   */
static UARTRegisters* UART4 = ((UARTRegisters*)0x40010000);
/* Implementation of the member variable UART5   */
static UARTRegisters* UART5 = ((UARTRegisters*)0x40011000);
/* Implementation of the member variable UART6   */
static UARTRegisters* UART6 = ((UARTRegisters*)0x40012000);
/* Implementation of the member variable UART7   */
static UARTRegisters* UART7 = ((UARTRegisters*)0x40013000);
/* Implementation of the member variable GPIOA   */
static GPIORegisters* GPIOA = ((GPIORegisters*)0x40004000);
/* Implementation of the member variable GPIOB   */
static GPIORegisters* GPIOB = ((GPIORegisters*)0x40005000);
/* Implementation of the member variable GPIOC   */
static GPIORegisters* GPIOC = ((GPIORegisters*)0x40006000);
/* Implementation of the member variable GPIOK   */
static GPIORegisters* GPIOK = ((GPIORegisters*)0x40061000);
/* Implementation of the member variable GPIOP   */
static GPIORegisters* GPIOP = ((GPIORegisters*)0x40065000);
 
 
/* Implementation of the function init   */
void SerialDriver_init(uint8 UartNumber,uint8 DataLength,uint32 BaudRate)
{
	uint32 mask_LCRH = (1 << 4);
	uint32 mask_IBRD = 0;
	uint32 mask_FBRD = 0;
	switch(DataLength)
	{
		case 5:
			mask_LCRH = mask_LCRH | (0x0 << 5);
			break;
		case 6:
			mask_LCRH = mask_LCRH | (0x1 << 5);
			break;
		case 7:
			mask_LCRH = mask_LCRH | (0x2 << 5);
			break;
		case 8:
			mask_LCRH = mask_LCRH | (0x3 << 5);
			break;
	}
	switch(BaudRate)
	{
		case 9600:
			mask_IBRD = 781;
			mask_FBRD = 16;
			break;
		case 19200:
			mask_IBRD = 390;
			mask_FBRD = 40;
			break;
		case 38400:
			mask_IBRD = 195;
			mask_FBRD = 20;
			break;
		case 57600:
			mask_IBRD = 130;
			mask_FBRD = 14;
			break;
		case 115200:
			mask_IBRD = 65;
			mask_FBRD = 7;
			break;
	}
	(*SYSCONTROL).RCGCUART = (*SYSCONTROL).RCGCUART | (1 << UartNumber);
	switch(UartNumber)
	{
		case 0:
			(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << 0);
			(*GPIOA).AFSEL = (*GPIOA).AFSEL | (1 << 0) | (1 << 1);
			(*GPIOA).DR2R = (*GPIOA).DR2R | (1 << 0) | (1 << 1);
			(*GPIOA).PCTL = (*GPIOA).PCTL | 0x00000011;
			(*GPIOA).DIR = (*GPIOA).DIR & ~(0x03);
			(*GPIOA).DEN = (*GPIOA).DEN | (1 << 0) | (1 << 1);
			(*UART0).CTL = (*UART0).CTL & ~(0x01);
			(*UART0).IBRD = mask_IBRD;
			(*UART0).FBRD = mask_FBRD;
			(*UART0).LCRH = (*UART0).LCRH | mask_LCRH;
			(*UART0).CC = 0;
			(*UART0).CTL = (*UART0).CTL | (1 << 0);
			break;
		case 1:
			(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << 1);
			(*GPIOB).AFSEL = (*GPIOB).AFSEL | (1 << 0) | (1 << 1);
			(*GPIOB).DR2R = (*GPIOB).DR2R | (1 << 0) | (1 << 1);
			(*GPIOB).PCTL = (*GPIOB).PCTL | 0x00000011;
			(*GPIOB).DIR = (*GPIOB).DIR & ~(0x03);
			(*GPIOB).DEN = (*GPIOB).DEN | (1 << 0) | (1 << 1);
			(*UART1).CTL = (*UART1).CTL & ~(0x01);
			(*UART1).IBRD = mask_IBRD;
			(*UART1).FBRD = mask_FBRD;
			(*UART1).LCRH = (*UART1).LCRH | mask_LCRH;
			(*UART1).CC = 0;
			(*UART1).CTL = (*UART1).CTL | (1 << 0);
			break;
		case 2:
			(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << 0);
			(*GPIOA).AFSEL = (*GPIOA).AFSEL | (1 << 6) | (1 << 7);
			(*GPIOA).DR2R = (*GPIOA).DR2R | (1 << 6) | (1 << 7);
			(*GPIOA).PCTL = (*GPIOA).PCTL | 0x11000000;
			(*GPIOA).DIR = (*GPIOA).DIR & ~(0xC0);
			(*GPIOA).DEN = (*GPIOA).DEN | (1 << 6) | (1 << 7);
			(*UART2).CTL = (*UART2).CTL & ~(0x01);
			(*UART2).IBRD = mask_IBRD;
			(*UART2).FBRD = mask_FBRD;
			(*UART2).LCRH = (*UART2).LCRH | mask_LCRH;
			(*UART2).CC = 0;
			(*UART2).CTL = (*UART2).CTL | (1 << 0);
			break;
		case 3:
			(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << 0);
			(*GPIOA).AFSEL = (*GPIOA).AFSEL | (1 << 4) | (1 << 5);
			(*GPIOA).DR2R = (*GPIOA).DR2R | (1 << 4) | (1 << 5);
			(*GPIOA).PCTL = (*GPIOA).PCTL | 0x00110000;
			(*GPIOA).DIR = (*GPIOA).DIR & ~(0x30);
			(*GPIOA).DEN = (*GPIOA).DEN | (1 << 4) | (1 << 5);
			(*UART3).CTL = (*UART3).CTL & ~(0x01);
			(*UART3).IBRD = mask_IBRD;
			(*UART3).FBRD = mask_FBRD;
			(*UART3).LCRH = (*UART3).LCRH | mask_LCRH;
			(*UART3).CC = 0;
			(*UART3).CTL = (*UART3).CTL | (1 << 0);
			break;
		case 4:
			(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << 9);
			(*GPIOK).AFSEL = (*GPIOK).AFSEL | (1 << 0) | (1 << 1);
			(*GPIOK).DR2R = (*GPIOK).DR2R | (1 << 0) | (1 << 1);
			(*GPIOK).PCTL = (*GPIOK).PCTL | 0x00000011;
			(*GPIOK).DIR = (*GPIOK).DIR & ~(0x03);
			(*GPIOK).DEN = (*GPIOK).DEN | (1 << 0) | (1 << 1);
			(*UART4).CTL = (*UART4).CTL & ~(0x01);
			(*UART4).IBRD = mask_IBRD;
			(*UART4).FBRD = mask_FBRD;
			(*UART4).LCRH = (*UART4).LCRH | mask_LCRH;
			(*UART4).CC = 0;
			(*UART4).CTL = (*UART4).CTL | (1 << 0);
			break;
		case 5:
			(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << 2);
			(*GPIOC).AFSEL = (*GPIOC).AFSEL | (1 << 6) | (1 << 7);
			(*GPIOC).DR2R = (*GPIOC).DR2R | (1 << 6) | (1 << 7);
			(*GPIOC).PCTL = (*GPIOC).PCTL | 0x11000000;
			(*GPIOC).DIR = (*GPIOC).DIR & ~(0xC0);
			(*GPIOC).DEN = (*GPIOC).DEN | (1 << 6) | (1 << 7);
			(*UART5).CTL = (*UART5).CTL & ~(0x01);
			(*UART5).IBRD = mask_IBRD;
			(*UART5).FBRD = mask_FBRD;
			(*UART5).LCRH = (*UART5).LCRH | mask_LCRH;
			(*UART5).CC = 0;
			(*UART5).CTL = (*UART5).CTL | (1 << 0);
			break;
		case 6:
			(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << 13);
			(*GPIOP).AFSEL = (*GPIOP).AFSEL | (1 << 0) | (1 << 1);
			(*GPIOP).DR2R = (*GPIOP).DR2R | (1 << 0) | (1 << 1);
			(*GPIOP).PCTL = (*GPIOP).PCTL | 0x00000011;
			(*GPIOP).DIR = (*GPIOP).DIR & ~(0x03);
			(*GPIOP).DEN = (*GPIOP).DEN | (1 << 0) | (1 << 1);
			(*UART6).CTL = (*UART6).CTL & ~(0x01);
			(*UART6).IBRD = mask_IBRD;
			(*UART6).FBRD = mask_FBRD;
			(*UART6).LCRH = (*UART6).LCRH | mask_LCRH;
			(*UART6).CC = 0;
			(*UART6).CTL = (*UART6).CTL | (1 << 0);
			break;
		case 7:
			(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << 2);
			(*GPIOC).AFSEL = (*GPIOC).AFSEL | (1 << 4) | (1 << 5);
			(*GPIOC).DR2R = (*GPIOC).DR2R | (1 << 4) | (1 << 5);
			(*GPIOC).PCTL = (*GPIOC).PCTL | 0x00110000;
			(*GPIOC).DIR = (*GPIOC).DIR & ~(0x30);
			(*GPIOC).DEN = (*GPIOC).DEN | (1 << 4) | (1 << 5);
			(*UART7).CTL = (*UART7).CTL & ~(0x01);
			(*UART7).IBRD = mask_IBRD;
			(*UART7).FBRD = mask_FBRD;
			(*UART7).LCRH = (*UART7).LCRH | mask_LCRH;
			(*UART7).CC = 0;
			(*UART7).CTL = (*UART7).CTL | (1 << 0);
			break;
	}
}
/* Implementation of the function sendCharacter   */
void SerialDriver_sendCharacter(uint8 character,uint8 UartNumber)
{
	switch(UartNumber)
	{
		case 0:
			while((*UART0).FR & (1 << 5))
				{
				}
			(*UART0).DR = character;
			break;
		case 1:
			while((*UART1).FR & (1 << 5))
				{
				}
			(*UART1).DR = character;
			break;
		case 2:
			while((*UART2).FR & (1 << 5))
				{
				}
			(*UART2).DR = character;
			break;
		case 3:
			while((*UART3).FR & (1 << 5))
				{
				}
			(*UART3).DR = character;
			break;
		case 4:
			while((*UART4).FR & (1 << 5))
				{
				}
			(*UART4).DR = character;
			break;
		case 5:
			while((*UART6).FR & (1 << 5))
				{
				}
			(*UART5).DR = character;
			break;
		case 6:
			while((*UART6).FR & (1 << 5))
				{
				}
			(*UART6).DR = character;
			break;
		case 7:
			while((*UART7).FR & (1 << 5))
				{
				}
			(*UART7).DR = character;
			break;
	}
}
/* Implementation of the function sendString   */
void SerialDriver_sendString(uint8 buffer[256],uint32 length,uint8 UartNumber)
{
	uint32 counter = 0;
	SerialDriver_sendCharacter(92, UartNumber);
	SerialDriver_sendCharacter(98, UartNumber);
	for(counter = 0;counter < length;counter = counter + 1)
		{
			SerialDriver_sendCharacter(buffer[counter], UartNumber);
			if(buffer[counter] == 92)
				SerialDriver_sendCharacter(92, UartNumber);
		}
	SerialDriver_sendCharacter(92, UartNumber);
	SerialDriver_sendCharacter(101, UartNumber);
}
 
 
 
 
