/**
 * OSARES Component low level implementation.
 *
 * Component name: SystemClock
 *
 * Generated: 2017-11-23T08:32:38.096+01:00[Europe/Prague]
 *
 */
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_GlobalConstants.h> 
#include<PIL_API_SystemClock.h> 
 
 
/* Implementation of the member variable SYSCONTROL   */
static SystemControlRegisters* SYSCONTROL = ((SystemControlRegisters*)0x400FE000);
 
 
/* Implementation of the function Init   */
uint32 SystemClock_Init()
{
	uint32 tmpRSCLKCFG = 0;
	uint32 TimeOut = 0;
	(*SYSCONTROL).MOSCCTL = (*SYSCONTROL).MOSCCTL & ~(1 << 2);
	(*SYSCONTROL).MOSCCTL = (*SYSCONTROL).MOSCCTL & ~(1 << 3);
	(*SYSCONTROL).RSCLKCFG = (*SYSCONTROL).RSCLKCFG | (0x3 << 20);
	(*SYSCONTROL).PLLFREQ0 = (*SYSCONTROL).PLLFREQ0 | 96;
	(*SYSCONTROL).PLLFREQ1 = (*SYSCONTROL).PLLFREQ1 | 4;
	(*SYSCONTROL).MEMTIM0 = (*SYSCONTROL).MEMTIM0 | (0x5 << 16) | (0x5 << 0) | (0x6 << 22) | (0x6 << 6);
	(*SYSCONTROL).MEMTIM0 = (*SYSCONTROL).MEMTIM0 & ~(1 << 21) & ~(1 << 5);
	(*SYSCONTROL).PLLFREQ0 = (*SYSCONTROL).PLLFREQ0 | (1 << 23);
	for(TimeOut = 32768;TimeOut > 0;TimeOut = TimeOut - 1)
		{
			if((*SYSCONTROL).PLLSTAT & (1 << 0))
				{
					break;
				}
		}
	if(TimeOut)
		{
			tmpRSCLKCFG = tmpRSCLKCFG | (0x3 << 24);
			tmpRSCLKCFG = tmpRSCLKCFG | (0x3 << 0);
			tmpRSCLKCFG = tmpRSCLKCFG | (1 << 28);
			tmpRSCLKCFG = tmpRSCLKCFG | (1 << 31);
			(*SYSCONTROL).RSCLKCFG = tmpRSCLKCFG;
			return 120000000;
		}
	else
		return 0;
}
 
 
 
 
