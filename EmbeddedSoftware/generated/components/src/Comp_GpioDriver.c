/**
 * OSARES Component low level implementation.
 *
 * Component name: GpioDriver
 *
 * Generated: 2017-11-23T08:32:38.082+01:00[Europe/Prague]
 *
 */
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_GlobalConstants.h> 
#include<PIL_API_GpioDriver.h> 
 
 
/* Implementation of the member variable SYSCONTROL   */
static SystemControlRegisters* SYSCONTROL = ((SystemControlRegisters*)0x400FE000);
/* Implementation of the member variable portA   */
static GPIORegisters* portA = ((GPIORegisters*)0x40058000);
/* Implementation of the member variable portB   */
static GPIORegisters* portB = ((GPIORegisters*)0x40059000);
/* Implementation of the member variable portC   */
static GPIORegisters* portC = ((GPIORegisters*)0x4005A000);
/* Implementation of the member variable portD   */
static GPIORegisters* portD = ((GPIORegisters*)0x4005B000);
/* Implementation of the member variable portE   */
static GPIORegisters* portE = ((GPIORegisters*)0x4005C000);
/* Implementation of the member variable portF   */
static GPIORegisters* portF = ((GPIORegisters*)0x4005D000);
/* Implementation of the member variable portG   */
static GPIORegisters* portG = ((GPIORegisters*)0x4005E000);
/* Implementation of the member variable portH   */
static GPIORegisters* portH = ((GPIORegisters*)0x4005F000);
/* Implementation of the member variable portJ   */
static GPIORegisters* portJ = ((GPIORegisters*)0x40060000);
/* Implementation of the member variable portK   */
static GPIORegisters* portK = ((GPIORegisters*)0x40061000);
/* Implementation of the member variable portL   */
static GPIORegisters* portL = ((GPIORegisters*)0x40062000);
/* Implementation of the member variable portM   */
static GPIORegisters* portM = ((GPIORegisters*)0x40063000);
/* Implementation of the member variable portN   */
static GPIORegisters* portN = ((GPIORegisters*)0x40064000);
/* Implementation of the member variable portP   */
static GPIORegisters* portP = ((GPIORegisters*)0x40065000);
/* Implementation of the member variable portQ   */
static GPIORegisters* portQ = ((GPIORegisters*)0x40066000);
 
 
/* Implementation of the function init   */
void GpioDriver_init(uint8 port,uint8 pin,uint8 isOutput,uint8 isDigital)
{
	(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << port);
	switch(port)
	{
		case 0:
			(*portA).DIR = (*portA).DIR | (isOutput << pin);
			(*portA).DEN = (*portA).DEN | (isDigital << pin);
			break;
		case 1:
			(*portB).DIR = (*portB).DIR | (isOutput << pin);
			(*portB).DEN = (*portB).DEN | (isDigital << pin);
			break;
		case 2:
			(*portC).DIR = (*portC).DIR | (isOutput << pin);
			(*portC).DEN = (*portC).DEN | (isDigital << pin);
			break;
		case 3:
			(*portD).DIR = (*portD).DIR | (isOutput << pin);
			(*portD).DEN = (*portD).DEN | (isDigital << pin);
			break;
		case 4:
			(*portE).DIR = (*portE).DIR | (isOutput << pin);
			(*portE).DEN = (*portE).DEN | (isDigital << pin);
			break;
		case 5:
			(*portF).DIR = (*portF).DIR | (isOutput << pin);
			(*portF).DEN = (*portF).DEN | (isDigital << pin);
			break;
		case 6:
			(*portG).DIR = (*portG).DIR | (isOutput << pin);
			(*portG).DEN = (*portG).DEN | (isDigital << pin);
			break;
		case 7:
			(*portH).DIR = (*portH).DIR | (isOutput << pin);
			(*portH).DEN = (*portH).DEN | (isDigital << pin);
			break;
		case 8:
			(*portJ).DIR = (*portJ).DIR | (isOutput << pin);
			(*portJ).DEN = (*portJ).DEN | (isDigital << pin);
			break;
		case 9:
			(*portK).DIR = (*portK).DIR | (isOutput << pin);
			(*portK).DEN = (*portK).DEN | (isDigital << pin);
			break;
		case 10:
			(*portL).DIR = (*portL).DIR | (isOutput << pin);
			(*portL).DEN = (*portL).DEN | (isDigital << pin);
			break;
		case 11:
			(*portM).DIR = (*portM).DIR | (isOutput << pin);
			(*portM).DEN = (*portM).DEN | (isDigital << pin);
			break;
		case 12:
			(*portN).DIR = (*portN).DIR | (isOutput << pin);
			(*portN).DEN = (*portN).DEN | (isDigital << pin);
			break;
		case 13:
			(*portP).DIR = (*portP).DIR | (isOutput << pin);
			(*portP).DEN = (*portP).DEN | (isDigital << pin);
			break;
		case 14:
			(*portQ).DIR = (*portQ).DIR | (isOutput << pin);
			(*portQ).DEN = (*portQ).DEN | (isDigital << pin);
			break;
	}
}
/* Implementation of the function set   */
void GpioDriver_set(uint8 port,uint8 pin)
{
	switch(port)
	{
		case 0:
			(*portA).DATA[1 << pin] = 1 << pin;
			break;
		case 1:
			(*portB).DATA[1 << pin] = 1 << pin;
			break;
		case 2:
			(*portC).DATA[1 << pin] = 1 << pin;
			break;
		case 3:
			(*portD).DATA[1 << pin] = 1 << pin;
			break;
		case 4:
			(*portE).DATA[1 << pin] = 1 << pin;
			break;
		case 5:
			(*portF).DATA[1 << pin] = 1 << pin;
			break;
		case 6:
			(*portG).DATA[1 << pin] = 1 << pin;
			break;
		case 7:
			(*portH).DATA[1 << pin] = 1 << pin;
			break;
		case 8:
			(*portJ).DATA[1 << pin] = 1 << pin;
			break;
		case 9:
			(*portK).DATA[1 << pin] = 1 << pin;
			break;
		case 10:
			(*portL).DATA[1 << pin] = 1 << pin;
			break;
		case 11:
			(*portM).DATA[1 << pin] = 1 << pin;
			break;
		case 12:
			(*portN).DATA[1 << pin] = 1 << pin;
			break;
		case 13:
			(*portP).DATA[1 << pin] = 1 << pin;
			break;
		case 14:
			(*portQ).DATA[1 << pin] = 1 << pin;
			break;
	}
}
/* Implementation of the function clear   */
void GpioDriver_clear(uint8 port,uint8 pin)
{
	switch(port)
	{
		case 0:
			(*portA).DATA[1 << pin] = 0;
			break;
		case 1:
			(*portB).DATA[1 << pin] = 0;
			break;
		case 2:
			(*portC).DATA[1 << pin] = 0;
			break;
		case 3:
			(*portD).DATA[1 << pin] = 0;
			break;
		case 4:
			(*portE).DATA[1 << pin] = 0;
			break;
		case 5:
			(*portF).DATA[1 << pin] = 0;
			break;
		case 6:
			(*portG).DATA[1 << pin] = 0;
			break;
		case 7:
			(*portH).DATA[1 << pin] = 0;
			break;
		case 8:
			(*portJ).DATA[1 << pin] = 0;
			break;
		case 9:
			(*portK).DATA[1 << pin] = 0;
			break;
		case 10:
			(*portL).DATA[1 << pin] = 0;
			break;
		case 11:
			(*portM).DATA[1 << pin] = 0;
			break;
		case 12:
			(*portN).DATA[1 << pin] = 0;
			break;
		case 13:
			(*portP).DATA[1 << pin] = 0;
			break;
		case 14:
			(*portQ).DATA[1 << pin] = 0;
			break;
	}
}
/* Implementation of the function toggle   */
void GpioDriver_toggle(uint8 port,uint8 pin)
{
	switch(port)
	{
		case 0:
			(*portA).DATA[1 << pin] = (1 << pin) - (*portA).DATA[1 << pin];
			break;
		case 1:
			(*portB).DATA[1 << pin] = (1 << pin) - (*portB).DATA[1 << pin];
			break;
		case 2:
			(*portC).DATA[1 << pin] = (1 << pin) - (*portC).DATA[1 << pin];
			break;
		case 3:
			(*portD).DATA[1 << pin] = (1 << pin) - (*portD).DATA[1 << pin];
			break;
		case 4:
			(*portE).DATA[1 << pin] = (1 << pin) - (*portE).DATA[1 << pin];
			break;
		case 5:
			(*portF).DATA[1 << pin] = (1 << pin) - (*portF).DATA[1 << pin];
			break;
		case 6:
			(*portG).DATA[1 << pin] = (1 << pin) - (*portG).DATA[1 << pin];
			break;
		case 7:
			(*portH).DATA[1 << pin] = (1 << pin) - (*portH).DATA[1 << pin];
			break;
		case 8:
			(*portJ).DATA[1 << pin] = (1 << pin) - (*portJ).DATA[1 << pin];
			break;
		case 9:
			(*portK).DATA[1 << pin] = (1 << pin) - (*portK).DATA[1 << pin];
			break;
		case 10:
			(*portL).DATA[1 << pin] = (1 << pin) - (*portL).DATA[1 << pin];
			break;
		case 11:
			(*portM).DATA[1 << pin] = (1 << pin) - (*portM).DATA[1 << pin];
			break;
		case 12:
			(*portN).DATA[1 << pin] = (1 << pin) - (*portN).DATA[1 << pin];
			break;
		case 13:
			(*portP).DATA[1 << pin] = (1 << pin) - (*portP).DATA[1 << pin];
			break;
		case 14:
			(*portQ).DATA[1 << pin] = (1 << pin) - (*portQ).DATA[1 << pin];
			break;
	}
}
/* Implementation of the function read   */
uint8 GpioDriver_read(uint8 port,uint8 pin)
{
	switch(port)
	{
		case 0:
			if((*portA).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 1:
			if((*portB).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 2:
			if((*portC).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 3:
			if((*portD).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 4:
			if((*portE).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 5:
			if((*portF).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 6:
			if((*portG).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 7:
			if((*portH).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 8:
			if((*portJ).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 9:
			if((*portK).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 10:
			if((*portL).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 11:
			if((*portM).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 12:
			if((*portN).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 13:
			if((*portP).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
		case 14:
			if((*portQ).DATA[1 << pin] == 0)
				return 0;
			else
				return 1;
			break;
	}
}
 
 
 
 
