/**
 * OSARES Component low level implementation.
 *
 * Component name: Communication
 *
 * Generated: 2017-11-23T08:32:38.104+01:00[Europe/Prague]
 *
 */
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_GlobalConstants.h> 
#include<PIL_API_Communication.h> 
 
 
/* Implementation of the member variable sensorValues   */
static uint16 sensorValues[8];
/* Implementation of the member variable parameters   */
static PhysicalParameters parameters;
/* Implementation of the member variable isSensorDriverInitialized   */
static uint8 isSensorDriverInitialized = 0;
/* Implementation of the member variable HumidityResistances   */
static float32 HumidityResistances[15][12];
/* Implementation of the member variable HumidityVoltages   */
static float32 HumidityVoltages[15][12];
 
 
/* Implementation of the function initArray   */
void Communication_initArray()
{
	HumidityResistances[0][0] = 0;
	HumidityResistances[0][1] = 15000;
	HumidityResistances[0][2] = 11000;
	HumidityResistances[0][3] = 8200;
	HumidityResistances[0][4] = 6300;
	HumidityResistances[0][5] = 4900;
	HumidityResistances[0][6] = 4000;
	HumidityResistances[0][7] = 3300;
	HumidityResistances[0][8] = 2800;
	HumidityResistances[0][9] = 2500;
	HumidityResistances[0][10] = 2200;
	HumidityResistances[0][11] = 2100;
	HumidityResistances[1][0] = 10000;
	HumidityResistances[1][1] = 7400;
	HumidityResistances[1][2] = 5300;
	HumidityResistances[1][3] = 3900;
	HumidityResistances[1][4] = 3000;
	HumidityResistances[1][5] = 2300;
	HumidityResistances[1][6] = 1800;
	HumidityResistances[1][7] = 1500;
	HumidityResistances[1][8] = 1300;
	HumidityResistances[1][9] = 1100;
	HumidityResistances[1][10] = 1000;
	HumidityResistances[1][11] = 920;
	HumidityResistances[2][0] = 4800;
	HumidityResistances[2][1] = 3400;
	HumidityResistances[2][2] = 2400;
	HumidityResistances[2][3] = 1800;
	HumidityResistances[2][4] = 1400;
	HumidityResistances[2][5] = 1100;
	HumidityResistances[2][6] = 870;
	HumidityResistances[2][7] = 710;
	HumidityResistances[2][8] = 600;
	HumidityResistances[2][9] = 510;
	HumidityResistances[2][10] = 450;
	HumidityResistances[2][11] = 410;
	HumidityResistances[3][0] = 2100;
	HumidityResistances[3][1] = 1500;
	HumidityResistances[3][2] = 1100;
	HumidityResistances[3][3] = 820;
	HumidityResistances[3][4] = 630;
	HumidityResistances[3][5] = 490;
	HumidityResistances[3][6] = 390;
	HumidityResistances[3][7] = 320;
	HumidityResistances[3][8] = 270;
	HumidityResistances[3][9] = 230;
	HumidityResistances[3][10] = 200;
	HumidityResistances[3][11] = 180;
	HumidityResistances[4][0] = 980;
	HumidityResistances[4][1] = 700;
	HumidityResistances[4][2] = 520;
	HumidityResistances[4][3] = 400;
	HumidityResistances[4][4] = 310;
	HumidityResistances[4][5] = 240;
	HumidityResistances[4][6] = 190;
	HumidityResistances[4][7] = 160;
	HumidityResistances[4][8] = 130;
	HumidityResistances[4][9] = 110;
	HumidityResistances[4][10] = 100;
	HumidityResistances[4][11] = 91;
	HumidityResistances[5][0] = 480;
	HumidityResistances[5][1] = 350;
	HumidityResistances[5][2] = 260;
	HumidityResistances[5][3] = 200;
	HumidityResistances[5][4] = 160;
	HumidityResistances[5][5] = 120;
	HumidityResistances[5][6] = 100;
	HumidityResistances[5][7] = 86;
	HumidityResistances[5][8] = 73;
	HumidityResistances[5][9] = 63;
	HumidityResistances[5][10] = 55;
	HumidityResistances[5][11] = 50;
	HumidityResistances[6][0] = 250;
	HumidityResistances[6][1] = 190;
	HumidityResistances[6][2] = 140;
	HumidityResistances[6][3] = 110;
	HumidityResistances[6][4] = 87;
	HumidityResistances[6][5] = 74;
	HumidityResistances[6][6] = 61;
	HumidityResistances[6][7] = 51;
	HumidityResistances[6][8] = 44;
	HumidityResistances[6][9] = 38;
	HumidityResistances[6][10] = 34;
	HumidityResistances[6][11] = 30;
	HumidityResistances[7][0] = 130;
	HumidityResistances[7][1] = 100;
	HumidityResistances[7][2] = 80;
	HumidityResistances[7][3] = 64;
	HumidityResistances[7][4] = 49;
	HumidityResistances[7][5] = 43;
	HumidityResistances[7][6] = 36;
	HumidityResistances[7][7] = 30;
	HumidityResistances[7][8] = 26;
	HumidityResistances[7][9] = 23;
	HumidityResistances[7][10] = 21;
	HumidityResistances[7][11] = 19;
	HumidityResistances[8][0] = 73;
	HumidityResistances[8][1] = 57;
	HumidityResistances[8][2] = 46;
	HumidityResistances[8][3] = 37;
	HumidityResistances[8][4] = 31;
	HumidityResistances[8][5] = 26;
	HumidityResistances[8][6] = 22;
	HumidityResistances[8][7] = 19;
	HumidityResistances[8][8] = 17;
	HumidityResistances[8][9] = 15;
	HumidityResistances[8][10] = 14;
	HumidityResistances[8][11] = 13;
	HumidityResistances[9][0] = 41;
	HumidityResistances[9][1] = 32;
	HumidityResistances[9][2] = 27;
	HumidityResistances[9][3] = 22;
	HumidityResistances[9][4] = 19;
	HumidityResistances[9][5] = 16;
	HumidityResistances[9][6] = 14;
	HumidityResistances[9][7] = 12;
	HumidityResistances[9][8] = 11;
	HumidityResistances[9][9] = 10;
	HumidityResistances[9][10] = 9.2;
	HumidityResistances[9][11] = 8.6;
	HumidityResistances[10][0] = 23;
	HumidityResistances[10][1] = 19;
	HumidityResistances[10][2] = 16;
	HumidityResistances[10][3] = 14;
	HumidityResistances[10][4] = 11.8;
	HumidityResistances[10][5] = 10.2;
	HumidityResistances[10][6] = 9.1;
	HumidityResistances[10][7] = 8.2;
	HumidityResistances[10][8] = 7.5;
	HumidityResistances[10][9] = 6.9;
	HumidityResistances[10][10] = 6.5;
	HumidityResistances[10][11] = 6.1;
	HumidityResistances[11][0] = 13;
	HumidityResistances[11][1] = 11;
	HumidityResistances[11][2] = 9.5;
	HumidityResistances[11][3] = 8.4;
	HumidityResistances[11][4] = 7.5;
	HumidityResistances[11][5] = 6.7;
	HumidityResistances[11][6] = 6.1;
	HumidityResistances[11][7] = 5.6;
	HumidityResistances[11][8] = 5.2;
	HumidityResistances[11][9] = 4.9;
	HumidityResistances[11][10] = 4.6;
	HumidityResistances[11][11] = 4.4;
	HumidityResistances[12][0] = 7.2;
	HumidityResistances[12][1] = 6.4;
	HumidityResistances[12][2] = 5.8;
	HumidityResistances[12][3] = 5.2;
	HumidityResistances[12][4] = 4.8;
	HumidityResistances[12][5] = 4.4;
	HumidityResistances[12][6] = 4.1;
	HumidityResistances[12][7] = 3.8;
	HumidityResistances[12][8] = 3.6;
	HumidityResistances[12][9] = 3.4;
	HumidityResistances[12][10] = 3.3;
	HumidityResistances[12][11] = 3.2;
	HumidityResistances[13][0] = 4;
	HumidityResistances[13][1] = 3.7;
	HumidityResistances[13][2] = 3.5;
	HumidityResistances[13][3] = 3.3;
	HumidityResistances[13][4] = 3;
	HumidityResistances[13][5] = 2.9;
	HumidityResistances[13][6] = 2.8;
	HumidityResistances[13][7] = 2.6;
	HumidityResistances[13][8] = 2.5;
	HumidityResistances[13][9] = 2.4;
	HumidityResistances[13][10] = 2.4;
	HumidityResistances[13][11] = 2.3;
	HumidityResistances[14][0] = 2.2;
	HumidityResistances[14][1] = 2.2;
	HumidityResistances[14][2] = 2.1;
	HumidityResistances[14][3] = 2;
	HumidityResistances[14][4] = 2;
	HumidityResistances[14][5] = 1.9;
	HumidityResistances[14][6] = 1.8;
	HumidityResistances[14][7] = 1.8;
	HumidityResistances[14][8] = 1.7;
	HumidityResistances[14][9] = 1.7;
	HumidityResistances[14][10] = 1.7;
	HumidityResistances[14][11] = 1.6;
}
/* Implementation of the function calculateVoltageMatrix   */
void Communication_calculateVoltageMatrix()
{
	uint8 row;
	uint8 col;
	Communication_initArray();
	for(row = 0;row < 15;row = row + 1)
		{
			for(col = 0;col < 12;col = col + 1)
				{
					HumidityVoltages[row][col] = 5 * 47 / (47 + HumidityResistances[row][col]);
				}
		}
}
/* Implementation of the function linearInterpolation   */
float32 Communication_linearInterpolation(float32 number1,float32 number2,float32 factor)
{
	return number1 + (number2 - number1) * factor / 5;
}
/* Implementation of the function calculateHumidity   */
float32 Communication_calculateHumidity(uint8 temperature,float32 measuredVoltage)
{
	uint8 row = 0;
	uint8 minimumRow = 0;
	float32 minimumValue = 5.0;
	float32 tmp = 0;
	float32 humidities[15];
	if(!isSensorDriverInitialized)
		Communication_calculateVoltageMatrix();
	for(row = 0;row < 15;row = row + 1)
		{
			if(temperature % 5)
				{
					humidities[row] = Communication_linearInterpolation(HumidityVoltages[row][temperature / 5 - 1], HumidityVoltages[row + 1][temperature / 5 - 1], temperature % 5);
				}
			else
				{
					humidities[row] = HumidityVoltages[row][temperature / 5 - 1];
				}
		}
	for(row = 0;row < 15;row = row + 1)
		{
			tmp = humidities[row] - measuredVoltage;
			if(tmp < 0)
				tmp = -tmp;
			if(tmp < minimumValue)
				{
					minimumValue = tmp;
					minimumRow = row;
				}
		}
	tmp = measuredVoltage - humidities[minimumRow];
	if(tmp > 0)
		{
			return 20 + (minimumRow * 5) + 5 * (measuredVoltage - humidities[minimumRow]) / (humidities[minimumRow + 1] - humidities[minimumRow]);
		}
	else
		{
			return 20 + (minimumRow * 5) + 5 * (measuredVoltage - humidities[minimumRow]) / (humidities[minimumRow] - humidities[minimumRow - 1]);
		}
}
/* Implementation of the function calculateSensorValues   */
void Communication_calculateSensorValues(uint8* temperature,uint8* humidity,uint8* motion)
{
	float32 humidityVoltage = sensorValues[1] * (3.3 / 4096);
	(*temperature) = (3.3) * (sensorValues[0] - sensorValues[2]) / (4096) * 100;
	(*humidity) = Communication_calculateHumidity((*temperature), humidityVoltage);
	(*motion) = sensorValues[3] > 4096 / 2;
}
/* Implementation of the function itoa   */
void Communication_itoa(uint8 value,uint8 valueBuffer[4])
{
	uint8 i = 4 - 1;
	if(value == 0)
		valueBuffer[0] = 0x30;
	while(value > 0)
		{
			valueBuffer[i] = (value % 10) + 0x30;
			i = i - 1;
			value = value / 10;
		}
}
/* Implementation of the function numberOfDigits   */
uint8 Communication_numberOfDigits(uint16 number,uint8 scale)
{
	uint8 i;
	i = 0;
	if(number == 0)
		return 1;
	while(number > 0)
		{
			i = i + 1;
			number = number / scale;
		}
	return i;
}
/* Implementation of the function sendMessageSerial   */
void Communication_sendMessageSerial(uint8 UartNumber,uint8 value,uint16 messageId,uint8 resource[32],uint8 resourceLength)
{
	uint8 valueNumberOfDigits;
	uint8 valueBuffer[4] = {0};
	valueNumberOfDigits = Communication_numberOfDigits(value, 10);
	Communication_itoa(value, valueBuffer);
	PIL_Call_Communication_coapFunctions_coapCreateMessage(2, 0, 3, messageId, valueNumberOfDigits, valueBuffer, 11, resourceLength, resource);
	PIL_Call_Communication_coapFunctions_coapSerializeMessage();
	PIL_Call_Communication_serialFunctions_SendString(PIL_Read_Communication_coapMessage_Serialized(), PIL_Read_Communication_coapMessage_Length(), UartNumber);
}
/* Implementation of the function GetPhysicalValues   */
void Communication_GetPhysicalValues(uint8 Adc,uint8 Sequencer)
{
	PIL_Call_Communication_adcFunctions_Measure(Adc, Sequencer);
	{
		uint16* __tmpdata;
		__tmpdata=PIL_Read_Communication_adcDatas_FIFOs();
		{
			uint32 __tmpi0;
			for (__tmpi0=0;__tmpi0<8;__tmpi0++)
					sensorValues[__tmpi0]=__tmpdata[__tmpi0];
		}
	}
;
	Communication_calculateSensorValues(&(parameters.Temperature), &(parameters.Humidity), &(parameters.Motion));
}
/* Implementation of the function timeoutHandling   */
void Communication_timeoutHandling()
{
	PIL_Call_Communication_gpioFunctions_Clear(3, 2);
	PIL_Call_Communication_gpioFunctions_Set(3, 2);
	PIL_Call_Communication_gpioFunctions_Set(1, 5);
}
/* Implementation of the function SendPhysicalValues   */
uint8 Communication_SendPhysicalValues(uint8 UartNumber)
{
	uint8 temp[32] = {84,101,109,112,101,114,97,116,117,114,101};
	uint8 hum[32] = {72,117,109,105,100,105,116,121};
	uint8 mot[32] = {77,111,116,105,111,110};
	uint16 uniqueMessageId = 0;
	uint32 timeoutCounter = 0xFFFFFFFF;
	uint8 oldTemperature = 0;
	uint8 oldHumidity = 0;
	uint8 isWifiSleeping = 1;
	uint8 isChanged = 0;
	oldTemperature = PIL_Call_Communication_hibernateFunctions_ReadData(0);
	oldHumidity = PIL_Call_Communication_hibernateFunctions_ReadData(1);
	uniqueMessageId = (1 << 8) + PIL_Call_Communication_hibernateFunctions_ReadData(2);
	if((oldTemperature != parameters.Temperature) | oldHumidity != parameters.Humidity | parameters.Motion)
		{
			PIL_Call_Communication_gpioFunctions_Clear(3, 2);
			PIL_Call_Communication_gpioFunctions_Set(3, 2);
			while(!(PIL_Call_Communication_gpioFunctions_Read(1, 4)))
				{
					timeoutCounter = timeoutCounter - 1;
					if(timeoutCounter == 0)
						{
							Communication_timeoutHandling();
							PIL_Call_Communication_gpioFunctions_Set(1, 5);
							while(1)
								{
								}
						}
				}
			Communication_sendMessageSerial(UartNumber, 84, uniqueMessageId, 0, 1);
			uniqueMessageId = uniqueMessageId + 1;
			PIL_Call_Communication_hibernateFunctions_WriteData(0, parameters.Temperature);
			Communication_sendMessageSerial(UartNumber, parameters.Temperature, uniqueMessageId, temp, 11);
			uniqueMessageId = uniqueMessageId + 1;
			PIL_Call_Communication_hibernateFunctions_WriteData(1, parameters.Humidity);
			Communication_sendMessageSerial(UartNumber, parameters.Humidity, uniqueMessageId, hum, 8);
			uniqueMessageId = uniqueMessageId + 1;
			Communication_sendMessageSerial(UartNumber, parameters.Motion, uniqueMessageId, mot, 6);
			uniqueMessageId = uniqueMessageId + 1;
			PIL_Call_Communication_hibernateFunctions_WriteData(2, uniqueMessageId);
		}
	PIL_Call_Communication_gpioFunctions_Set(3, 2);
	while(PIL_Call_Communication_gpioFunctions_Read(1, 4))
		{
		}
	PIL_Call_Communication_gpioFunctions_Set(1, 5);
}
 
 
 
 
