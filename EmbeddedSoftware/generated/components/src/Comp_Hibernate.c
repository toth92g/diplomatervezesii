/**
 * OSARES Component low level implementation.
 *
 * Component name: Hibernate
 *
 * Generated: 2017-11-23T08:32:38.116+01:00[Europe/Prague]
 *
 */
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_GlobalConstants.h> 
#include<PIL_API_Hibernate.h> 
 
 
/* Implementation of the member variable SYSCONTROL   */
static SystemControlRegisters* SYSCONTROL = ((SystemControlRegisters*)0x400FE000);
/* Implementation of the member variable HIB   */
static HibernationRegisters* HIB = ((HibernationRegisters*)0x400FC000);
 
 
/* Implementation of the function writeComplete   */
void Hibernate_writeComplete()
{
	while(!((*HIB).CTL & (1 << 31)))
		{
		}
}
/* Implementation of the function Init   */
void Hibernate_Init()
{
	uint32 tmpCTL = 0;
	uint32 tmpIC = 0;
	(*SYSCONTROL).RCGCHIB = (1 << 0);
	tmpCTL = tmpCTL | (1 << 6);
	tmpCTL = tmpCTL | (1 << 0);
	tmpCTL = tmpCTL | (1 << 3);
	tmpCTL = tmpCTL | (1 << 4);
	tmpIC = tmpIC | (1 << 0);
	tmpIC = tmpIC | (1 << 7);
	tmpIC = tmpIC | (1 << 3);
	(*HIB).CTL = tmpCTL;
	Hibernate_writeComplete();
	(*HIB).IC = tmpIC;
	Hibernate_writeComplete();
	tmpCTL = (*HIB).CTL;
	tmpCTL = tmpCTL | (1 << 8);
	tmpCTL = tmpCTL | (1 << 30);
	(*HIB).CTL = tmpCTL;
	Hibernate_writeComplete();
}
/* Implementation of the function setWakeUpTime   */
void Hibernate_setWakeUpTime(uint32 time)
{
	(*HIB).LOCK = 0xA3359554;
	Hibernate_writeComplete();
	(*HIB).RTCLD = 0;
	Hibernate_writeComplete();
	(*HIB).LOCK = 0;
	Hibernate_writeComplete();
	(*HIB).RTCM0 = time;
	Hibernate_writeComplete();
}
/* Implementation of the function hibernateRequest   */
void Hibernate_hibernateRequest()
{
	(*HIB).CTL = (*HIB).CTL | (1 << 1);
	Hibernate_writeComplete();
}
/* Implementation of the function Hibernate   */
void Hibernate_Hibernate(uint32 time)
{
	Hibernate_setWakeUpTime(time);
	Hibernate_hibernateRequest();
}
/* Implementation of the function readData   */
uint8 Hibernate_readData(uint8 byteNumber)
{
	return ((*HIB).DATA[byteNumber / 4] >> 8 * (byteNumber % 4));
}
/* Implementation of the function writeData   */
void Hibernate_writeData(uint8 byteNumber,uint8 dataByte)
{
	uint32 tmpNumber = (*HIB).DATA[byteNumber / 4];
	switch(byteNumber % 4)
	{
		case 0:
			tmpNumber = tmpNumber & 0xFFFFFF00;
			tmpNumber = tmpNumber | dataByte;
			break;
		case 1:
			tmpNumber = tmpNumber & 0xFFFF00FF;
			tmpNumber = tmpNumber | (dataByte << 8);
			break;
		case 2:
			tmpNumber = tmpNumber & 0xFF00FFFF;
			tmpNumber = tmpNumber | (dataByte << 16);
			break;
		case 3:
			tmpNumber = tmpNumber & 0x00FFFFFF;
			tmpNumber = tmpNumber | (dataByte << 24);
			break;
	}
	(*HIB).DATA[byteNumber / 4] = tmpNumber;
	Hibernate_writeComplete();
}
 
 
 
 
