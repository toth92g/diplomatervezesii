/**
 * OSARES Component low level implementation.
 *
 * Component name: CoAP
 *
 * Generated: 2017-11-23T08:32:38.146+01:00[Europe/Prague]
 *
 */
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_GlobalConstants.h> 
#include<PIL_API_CoAP.h> 
 
 
/* Implementation of the member variable VERSION   */
static uint8 VERSION = 1;
/* Implementation of the member variable IfMatch   */
static uint8 IfMatch = 1;
/* Implementation of the member variable UriHost   */
static uint8 UriHost = 3;
/* Implementation of the member variable Etag   */
static uint8 Etag = 4;
/* Implementation of the member variable IfNoneMatch   */
static uint8 IfNoneMatch = 5;
/* Implementation of the member variable UriPort   */
static uint8 UriPort = 7;
/* Implementation of the member variable LocationPath   */
static uint8 LocationPath = 8;
/* Implementation of the member variable UriPath   */
static uint8 UriPath = 11;
/* Implementation of the member variable ContentFormat   */
static uint8 ContentFormat = 12;
/* Implementation of the member variable MaxAge   */
static uint8 MaxAge = 14;
/* Implementation of the member variable UriQuery   */
static uint8 UriQuery = 15;
/* Implementation of the member variable Accept   */
static uint8 Accept = 17;
/* Implementation of the member variable LocationQuery   */
static uint8 LocationQuery = 20;
/* Implementation of the member variable ProxyUri   */
static uint8 ProxyUri = 35;
/* Implementation of the member variable ProxyScheme   */
static uint8 ProxyScheme = 39;
/* Implementation of the member variable Size1   */
static uint8 Size1 = 60;
/* Implementation of the member variable GET   */
static uint8 GET = 1;
/* Implementation of the member variable POST   */
static uint8 POST = 2;
/* Implementation of the member variable PUT   */
static uint8 PUT = 3;
/* Implementation of the member variable DELETE   */
static uint8 DELETE = 4;
/* Implementation of the member variable CONFIRMABLE   */
static uint8 CONFIRMABLE = 1;
/* Implementation of the member variable NONCONFIRMABLE   */
static uint8 NONCONFIRMABLE = 2;
/* Implementation of the member variable ACKNOWLEDGEMENT   */
static uint8 ACKNOWLEDGEMENT = 3;
/* Implementation of the member variable RESET   */
static uint8 RESET = 4;
/* Implementation of the member variable option   */
static coapOption option;
/* Implementation of the member variable message   */
static coapMessage message;
 
 
/* Implementation of the function CreateOption   */
void CoAP_CreateOption(uint8 delta,uint8 resourceLength,uint8 resource[32])
{
	uint8 i;
	option.delta = delta << 4;
	option.length = resourceLength;
	for(i = 0;i < resourceLength;i = i + 1)
		option.value[i] = resource[i];
	option.value[resourceLength] = 0;
}
/* Implementation of the function CreateMessage   */
void CoAP_CreateMessage(uint8 type,uint8 tokenLength,uint8 code,uint16 messageId,uint8 payloadLength,uint8 payload[4],uint8 optionDelta,uint8 optionResourceLength,uint8 optionResource[32])
{
	uint8 i;
	uint8 j;
	CoAP_CreateOption(optionDelta, optionResourceLength, optionResource);
	message.version = VERSION;
	message.type = type;
	message.tokenLength = tokenLength;
	message.code = code;
	message.messageId = messageId;
	message.token = 0;
	message.payloadMarker = 0xFF;
	j = 0;
	while(payload[j] == 0)
		j = j + 1;
	for(i = 0;i < payloadLength;i = i + 1)
		message.payload[i] = payload[i + j];
	message.messageLength = 1 + tokenLength + 1 + 2 + option.length + 1 + 1 + payloadLength;
}
/* Implementation of the function SerializeMessage   */
void CoAP_SerializeMessage()
{
	uint8 i;
	uint8 dummyVector[100];
	for(i = 0;i < 100;i = i + 1)
		dummyVector[i] = 0;
	PIL_Write_CoAP_publicData_Length(message.messageLength);;
	dummyVector[0] = message.version << 6 | message.type << 4 | message.tokenLength;
	dummyVector[1] = message.code;
	dummyVector[2] = message.messageId >> 8;
	dummyVector[3] = message.messageId;
	dummyVector[4] = option.delta | option.length;
	for(i = 0;i < option.length;i = i + 1)
		{
			dummyVector[5 + i] = option.value[i];
		}
	dummyVector[5 + option.length] = 0xff;
	for(i = 0;i < message.messageLength - (5 + option.length + 1);i = i + 1)
		{
			dummyVector[5 + option.length + 1 + i] = message.payload[i];
		}
	PIL_Write_CoAP_publicData_Serialized(dummyVector);;
}
 
 
 
 
