/**
 * OSARES Component low level implementation.
 *
 * Component name: AdcDriver
 *
 * Generated: 2017-11-23T08:32:38.128+01:00[Europe/Prague]
 *
 */
 
#include<Osares_BaseTypes.h> 
#include<PIL_ApplicationTypes.h> 
#include<PIL_GlobalConstants.h> 
#include<PIL_API_AdcDriver.h> 
 
 
/* Implementation of the member variable SYSCONTROL   */
static SystemControlRegisters* SYSCONTROL = ((SystemControlRegisters*)0x400FE000);
/* Implementation of the member variable ADC0   */
static ADCRegisters* ADC0 = ((ADCRegisters*)0x40038000);
/* Implementation of the member variable ADC1   */
static ADCRegisters* ADC1 = ((ADCRegisters*)0x40039000);
/* Implementation of the member variable GPIOB   */
static GPIORegisters* GPIOB = ((GPIORegisters*)0x40005000);
/* Implementation of the member variable GPIOD   */
static GPIORegisters* GPIOD = ((GPIORegisters*)0x40007000);
/* Implementation of the member variable GPIOE   */
static GPIORegisters* GPIOE = ((GPIORegisters*)0x40024000);
/* Implementation of the member variable GPIOK   */
static GPIORegisters* GPIOK = ((GPIORegisters*)0x40061000);
 
 
/* Implementation of the function EnableSequencer   */
void AdcDriver_EnableSequencer(ADCRegisters* ADC,uint8 Sequencer)
{
	(*ADC).ACTSS = (*ADC).ACTSS | (1 << Sequencer);
}
/* Implementation of the function DisableSequencer   */
void AdcDriver_DisableSequencer(ADCRegisters* ADC,uint8 Sequencer)
{
	(*ADC).ACTSS = (*ADC).ACTSS & ~(1 << Sequencer);
}
/* Implementation of the function SequencerInputConfiguration   */
void AdcDriver_SequencerInputConfiguration(ADCRegisters* ADC,uint8 Sequencer,uint8 Input0,uint8 Input1,uint8 Input2,uint8 Input3,uint8 Input4,uint8 Input5,uint8 Input6,uint8 Input7)
{
	(*ADC).EMUX = 0;
	switch(Sequencer)
	{
		case 0:
			if(Input0 > 15)
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 | (1 << 0);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input0 - 16) << 0);
				}
			else
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 & ~(1 << 0);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input0) << 0);
				}
			if(Input1 > 15)
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 | (1 << 4);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input1 - 16) << 4);
				}
			else
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 & ~(1 << 4);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input1) << 4);
				}
			if(Input2 > 15)
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 | (1 << 8);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input2 - 16) << 8);
				}
			else
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 & ~(1 << 8);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input2) << 8);
				}
			if(Input3 > 15)
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 | (1 << 12);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input3 - 16) << 12);
				}
			else
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 & ~(1 << 12);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input3) << 12);
				}
			if(Input4 > 15)
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 | (1 << 16);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input4 - 16) << 16);
				}
			else
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 & ~(1 << 16);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input4) << 16);
				}
			if(Input5 > 15)
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 | (1 << 20);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input5 - 16) << 20);
				}
			else
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 & ~(1 << 20);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input5) << 20);
				}
			if(Input6 > 15)
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 | (1 << 24);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input6 - 16) << 24);
				}
			else
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 & ~(1 << 24);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input6) << 24);
				}
			if(Input7 > 15)
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 | (1 << 28);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input7 - 16) << 28);
				}
			else
				{
					(*ADC).SSEMUX0 = (*ADC).SSEMUX0 & ~(1 << 28);
					(*ADC).SSMUX0 = (*ADC).SSMUX0 | ((Input7) << 28);
				}
			(*ADC).SSCTL0 = (*ADC).SSCTL0 | (1 << 30);
			(*ADC).SSCTL0 = (*ADC).SSCTL0 | (1 << 29);
			break;
		case 1:
			if(Input0 > 15)
				{
					(*ADC).SSEMUX1 = (*ADC).SSEMUX1 | (1 << 0);
					(*ADC).SSMUX1 = (*ADC).SSMUX1 | ((Input0 - 16) << 0);
				}
			else
				{
					(*ADC).SSEMUX1 = (*ADC).SSEMUX1 & ~(1 << 0);
					(*ADC).SSMUX1 = (*ADC).SSMUX1 | ((Input0) << 0);
				}
			if(Input1 > 15)
				{
					(*ADC).SSEMUX1 = (*ADC).SSEMUX1 | (1 << 4);
					(*ADC).SSMUX1 = (*ADC).SSMUX1 | ((Input1 - 16) << 4);
				}
			else
				{
					(*ADC).SSEMUX1 = (*ADC).SSEMUX1 & ~(1 << 4);
					(*ADC).SSMUX1 = (*ADC).SSMUX1 | ((Input1) << 4);
				}
			if(Input2 > 15)
				{
					(*ADC).SSEMUX1 = (*ADC).SSEMUX1 | (1 << 8);
					(*ADC).SSMUX1 = (*ADC).SSMUX1 | ((Input2 - 16) << 8);
				}
			else
				{
					(*ADC).SSEMUX1 = (*ADC).SSEMUX1 & ~(1 << 8);
					(*ADC).SSMUX1 = (*ADC).SSMUX1 | ((Input2) << 8);
				}
			if(Input3 > 15)
				{
					(*ADC).SSEMUX1 = (*ADC).SSEMUX1 | (1 << 12);
					(*ADC).SSMUX1 = (*ADC).SSMUX1 | ((Input3 - 16) << 12);
				}
			else
				{
					(*ADC).SSEMUX1 = (*ADC).SSEMUX1 & ~(1 << 12);
					(*ADC).SSMUX1 = (*ADC).SSMUX1 | ((Input3) << 12);
				}
			(*ADC).SSCTL1 = (*ADC).SSCTL1 | (1 << 14);
			(*ADC).SSCTL1 = (*ADC).SSCTL1 | (1 << 13);
			break;
		case 2:
			if(Input0 > 15)
				{
					(*ADC).SSEMUX2 = (*ADC).SSEMUX2 | (1 << 0);
					(*ADC).SSMUX2 = (*ADC).SSMUX2 | ((Input0 - 16) << 0);
				}
			else
				{
					(*ADC).SSEMUX2 = (*ADC).SSEMUX2 & ~(1 << 0);
					(*ADC0).SSMUX2 = (*ADC).SSMUX2 | ((Input0) << 0);
				}
			if(Input1 > 15)
				{
					(*ADC).SSEMUX2 = (*ADC).SSEMUX2 | (1 << 4);
					(*ADC).SSMUX2 = (*ADC).SSMUX2 | ((Input1 - 16) << 4);
				}
			else
				{
					(*ADC).SSEMUX2 = (*ADC).SSEMUX2 & ~(1 << 4);
					(*ADC).SSMUX2 = (*ADC).SSMUX2 | ((Input1) << 4);
				}
			if(Input2 > 15)
				{
					(*ADC).SSEMUX2 = (*ADC).SSEMUX2 | (1 << 8);
					(*ADC).SSMUX2 = (*ADC).SSMUX2 | ((Input2 - 16) << 8);
				}
			else
				{
					(*ADC).SSEMUX2 = (*ADC).SSEMUX2 & ~(1 << 8);
					(*ADC).SSMUX2 = (*ADC).SSMUX2 | ((Input2) << 8);
				}
			if(Input3 > 15)
				{
					(*ADC).SSEMUX2 = (*ADC).SSEMUX2 | (1 << 12);
					(*ADC).SSMUX2 = (*ADC).SSMUX2 | ((Input3 - 16) << 12);
				}
			else
				{
					(*ADC).SSEMUX2 = (*ADC).SSEMUX2 & ~(1 << 12);
					(*ADC).SSMUX2 = (*ADC).SSMUX2 | ((Input3) << 12);
				}
			(*ADC).SSCTL2 = (*ADC).SSCTL2 | (1 << 14);
			(*ADC).SSCTL2 = (*ADC).SSCTL2 | (1 << 13);
			break;
		case 3:
			if(Input0 > 15)
				{
					(*ADC).SSEMUX3 = (*ADC).SSEMUX3 | (1 << 0);
					(*ADC).SSMUX3 = (*ADC).SSMUX3 | ((Input0 - 16) << 0);
				}
			else
				{
					(*ADC).SSEMUX3 = (*ADC).SSEMUX3 & ~(1 << 0);
					(*ADC).SSMUX3 = (*ADC).SSMUX3 | ((Input0) << 0);
				}
			(*ADC).SSCTL3 = (*ADC).SSCTL3 | (1 << 2);
			(*ADC).SSCTL3 = (*ADC).SSCTL3 | (1 << 1);
			break;
	}
}
/* Implementation of the function EnableSequencerInterrupt   */
void AdcDriver_EnableSequencerInterrupt(ADCRegisters* ADC,uint8 Sequencer)
{
	(*ADC).IM = (*ADC).IM | (1 << Sequencer);
}
/* Implementation of the function DisableSequencerInterrupt   */
void AdcDriver_DisableSequencerInterrupt(ADCRegisters* ADC,uint8 Sequencer)
{
	(*ADC).IM = (*ADC).IM & ~(1 << Sequencer);
}
/* Implementation of the function SetTiming   */
void AdcDriver_SetTiming(ADCRegisters* ADC,uint32 ClockDivisionRate)
{
	(*ADC).PC = 0x07;
	(*ADC).CC = (ClockDivisionRate - 1) << 4;
}
/* Implementation of the function ConfigGpios   */
void AdcDriver_ConfigGpios(GPIORegisters* GPIO,uint8 gpio,uint8 pin)
{
	(*SYSCONTROL).RCGCGPIO = (*SYSCONTROL).RCGCGPIO | (1 << gpio);
	(*GPIO).AFSEL = (*GPIO).AFSEL | (1 << pin);
	(*GPIO).AMSEL = (*GPIO).AMSEL | (1 << pin);
	(*GPIO).DIR = (*GPIO).DIR & ~(1 << pin);
	(*GPIO).DEN = (*GPIO).DEN & ~(1 << pin);
}
/* Implementation of the function ConfigInputPin   */
void AdcDriver_ConfigInputPin(uint8 input)
{
	switch(input)
	{
		case 0:
			AdcDriver_ConfigGpios(&((*GPIOE)), 4, 3);
			break;
		case 1:
			AdcDriver_ConfigGpios(&((*GPIOE)), 4, 2);
			break;
		case 2:
			AdcDriver_ConfigGpios(&((*GPIOE)), 4, 1);
			break;
		case 3:
			AdcDriver_ConfigGpios(&((*GPIOE)), 4, 0);
			break;
		case 4:
			AdcDriver_ConfigGpios(&((*GPIOD)), 3, 7);
			break;
		case 5:
			AdcDriver_ConfigGpios(&((*GPIOD)), 3, 6);
			break;
		case 6:
			AdcDriver_ConfigGpios(&((*GPIOD)), 3, 5);
			break;
		case 7:
			AdcDriver_ConfigGpios(&((*GPIOD)), 3, 4);
			break;
		case 8:
			AdcDriver_ConfigGpios(&((*GPIOE)), 4, 5);
			break;
		case 9:
			AdcDriver_ConfigGpios(&((*GPIOE)), 4, 4);
			break;
		case 10:
			AdcDriver_ConfigGpios(&((*GPIOB)), 1, 4);
			break;
		case 11:
			AdcDriver_ConfigGpios(&((*GPIOB)), 1, 5);
			break;
		case 12:
			AdcDriver_ConfigGpios(&((*GPIOD)), 3, 3);
			break;
		case 13:
			AdcDriver_ConfigGpios(&((*GPIOD)), 3, 2);
			break;
		case 14:
			AdcDriver_ConfigGpios(&((*GPIOD)), 3, 1);
			break;
		case 15:
			AdcDriver_ConfigGpios(&((*GPIOD)), 3, 0);
			break;
		case 16:
			AdcDriver_ConfigGpios(&((*GPIOK)), 9, 0);
			break;
		case 17:
			AdcDriver_ConfigGpios(&((*GPIOK)), 9, 1);
			break;
		case 18:
			AdcDriver_ConfigGpios(&((*GPIOK)), 9, 2);
			break;
		case 19:
			AdcDriver_ConfigGpios(&((*GPIOK)), 9, 3);
			break;
	}
}
/* Implementation of the function SampleAveraging   */
void AdcDriver_SampleAveraging(ADCRegisters* ADC,uint8 overSamplingPower)
{
	(*ADC).SAC = overSamplingPower;
}
/* Implementation of the function init   */
void AdcDriver_init(uint8 AdcNumber,uint8 Sequencer,uint32 ClockDivisionRate,uint8 HardwareAveraging,uint8 Input0,uint8 Input1,uint8 Input2,uint8 Input3,uint8 Input4,uint8 Input5,uint8 Input6,uint8 Input7)
{
	(*SYSCONTROL).RCGCADC = (1 << AdcNumber);
	AdcDriver_ConfigInputPin(Input0);
	AdcDriver_ConfigInputPin(Input1);
	AdcDriver_ConfigInputPin(Input2);
	AdcDriver_ConfigInputPin(Input3);
	AdcDriver_ConfigInputPin(Input4);
	AdcDriver_ConfigInputPin(Input5);
	AdcDriver_ConfigInputPin(Input6);
	AdcDriver_ConfigInputPin(Input7);
	switch(AdcNumber)
	{
		case 0:
			AdcDriver_DisableSequencer(&((*ADC0)), Sequencer);
			AdcDriver_SequencerInputConfiguration(&((*ADC0)), Sequencer, Input0, Input1, Input2, Input3, Input4, Input5, Input6, Input7);
			AdcDriver_EnableSequencerInterrupt(&((*ADC0)), Sequencer);
			AdcDriver_SetTiming(&((*ADC0)), ClockDivisionRate);
			AdcDriver_EnableSequencer(&((*ADC0)), Sequencer);
			AdcDriver_SampleAveraging(&((*ADC0)), HardwareAveraging);
			break;
		case 1:
			AdcDriver_DisableSequencer(&((*ADC1)), Sequencer);
			AdcDriver_SequencerInputConfiguration(&((*ADC1)), Sequencer, Input0, Input1, Input2, Input3, Input4, Input5, Input6, Input7);
			AdcDriver_EnableSequencerInterrupt(&((*ADC1)), Sequencer);
			AdcDriver_SetTiming(&((*ADC1)), ClockDivisionRate);
			AdcDriver_EnableSequencer(&((*ADC1)), Sequencer);
			AdcDriver_SampleAveraging(&((*ADC1)), HardwareAveraging);
			break;
	}
}
/* Implementation of the function measurementHelper   */
void AdcDriver_measurementHelper(ADCRegisters* ADC,uint8 Sequencer)
{
	uint8 i = 0;
	uint16 dummyVector[8] = {0,0,0,0,0,0,0,0};
	switch(Sequencer)
	{
		case 0:
			(*ADC).PSSI = (*ADC).PSSI | (1 << Sequencer) | (1 << 31);
			while(!((*ADC).RIS & (1 << Sequencer)))
				{
				}
			for(i = 0;i < 8;i = i + 1)
				{
					dummyVector[i] = (*ADC).SSFIFO0 & 0x0000fff;
				}
			break;
		case 1:
			(*ADC).PSSI = (*ADC).PSSI | (1 << Sequencer) | (1 << 31);
			while(!((*ADC).RIS & (1 << Sequencer)))
				{
				}
			for(i = 0;i < 4;i = i + 1)
				{
					dummyVector[i] = (*ADC).SSFIFO1 & 0x0000fff;
				}
			break;
		case 2:
			(*ADC).PSSI = (*ADC).PSSI | (1 << Sequencer) | (1 << 31);
			while(!((*ADC).RIS & (1 << Sequencer)))
				{
				}
			for(i = 0;i < 4;i = i + 1)
				{
					dummyVector[i] = (*ADC).SSFIFO2 & 0x0000fff;
				}
			break;
		case 3:
			(*ADC).PSSI = (*ADC).PSSI | (1 << Sequencer) | (1 << 31);
			while(!((*ADC).RIS & (1 << Sequencer)))
				{
				}
			dummyVector[0] = (*ADC).SSFIFO3 & 0x0000fff;
	}
	PIL_Write_AdcDriver_publicDatas_FIFOs(dummyVector);;
}
/* Implementation of the function measure   */
void AdcDriver_measure(uint8 AdcNumber,uint8 Sequencer)
{
	switch(AdcNumber)
	{
		case 0:
			AdcDriver_measurementHelper(&((*ADC0)), Sequencer);
			break;
		case 1:
			AdcDriver_measurementHelper(&((*ADC1)), Sequencer);
			break;
	}
}
 
 
 
 
