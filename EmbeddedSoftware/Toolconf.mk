# C compiler
CC := /c/ti/ccsv7/tools/compiler/gcc-arm-none-eabi-4_9-2015q3/bin/arm-none-eabi-gcc 
# linker
LD := /c/ti/ccsv7/tools/compiler/gcc-arm-none-eabi-4_9-2015q3/bin/arm-none-eabi-gcc 

# C flags
CFLAGS := $(INCLUDES)
CFLAGS += -Dgcc -Wall
CFLAGS += -g -mcpu=cortex-m4 -march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DPART_TM4C129ENCPDT
CFLAGS += -ffunction-sections -fdata-sections -gstrict-dwarf

# C/C++ flags
CPPFLAGS := -g -Wall -Wextra -pedantic

# linker flags
LDFLAGS :=
LDFLAGS += -Wall
LDFLAGS += -l"c" -l"gcc" -l"nosys"
LDFLAGS += -T tm4c129encpdt.ld  -eResetISR -nostartfiles
LDFLAGS += -mthumb -mfloat-abi=hard -march=armv7e-m -mfpu=fpv4-sp-d16 -DPART_TM4C129ENCPDT -ffunction-sections -fdata-sections -g -gstrict-dwarf -Wall -Wl,-Map,"GCCDemoProject.map" 

# flags required for dependency generation; passed to compilers
DEPFLAGS = -MT $@ -MD -MP -MF $(DEPDIR)/$*.Td