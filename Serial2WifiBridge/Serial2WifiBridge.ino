#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char* ssid = "ExampleSSID";
const char* password = "ExamplePW";

unsigned int localPort = 49153;
IPAddress serverIP(192, 168, 1, 100); // Router: 192.168.1.150

#define maximumMessageSize 255
#define TIMEOUT 10
WiFiClient client;
WiFiUDP port;

String message;
char puffer[maximumMessageSize];

char temp;
int state = 0;
int indexer = 0;
void setup() 
{
  Serial.begin(9600);
  pinMode(D8, OUTPUT);
  pinMode(D7, INPUT);
  digitalWrite(D8, LOW);
  WiFi.begin(ssid, password);
  port.begin(5683);

  while(WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }

  digitalWrite(D8, HIGH);
}

void loop() 
{
  sendSerial();
  
  delay(500);

  if(digitalRead(D7))
    ESP.deepSleep(10);  // 10 usec
}

void sendSerial()
{
  while(Serial.available() > 0)
  {
    temp = Serial.read();
    switch(state)
    {
      case 0:
        if(temp == '\\')    // IDLE state
          state = 1;
        break;
      case 1:    
        if(temp == 'b')     // Begin marker detected
        {
            indexer = 0;
            state = 2;
        }
        else
        {
          state = 0;
          indexer = 0;
        }
        break;
      case 2:             // Incoming data
        if(temp == '\\')
          state = 3;
        else
          puffer[indexer] = temp;
          indexer++;
        break;
      case 3:             // Escape marker in the data
        switch(temp)
        {
          case 'e':       // String finished
            puffer[indexer] = '\0';
            indexer++;
            sendMessage();
            state = 0;
            break;
          case 'b':       // New packet started in the middle of an other one
            state = 2;
            indexer = 0;
            break;
          case '\\':
            puffer[indexer] = temp;
            indexer++;
            state = 2;
            break;
          default:
            puffer[indexer] = temp;
            indexer++;
            state = 2;
            break;            
        }
        break;
    }
  }
}

void sendMessage() 
{
  port.beginPacket(serverIP,localPort);
  port.write(puffer);
  port.endPacket();

  digitalWrite(D8, LOW);
}

