Serial2WifiBridge version v1.0 23/11/2017	
Created by : Gabor Toth


GENERAL INFORMATION
-------------------

- The software gets a byte stream via UART and sends these data in an UDP message.

HARDWARE REQUIREMENTS
---------------------

- ESP8266 / NodeMCU WiFi module